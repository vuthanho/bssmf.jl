#!/usr/bin/env bash

wget -nc https://gitlab.com/vuthanho/data/-/raw/main/mnist.mat
mkdir ml-1m
wget -nc -P ml-1m/ https://gitlab.com/vuthanho/data/-/raw/main/ml-1m/data_tr.mat
wget -nc -P ml-1m/ https://gitlab.com/vuthanho/data/-/raw/main/ml-1m/data_te.mat
mkdir ml-100k
wget -nc -P ml-100k/ https://gitlab.com/vuthanho/data/-/raw/main/ml-100k/data_tr.mat
wget -nc -P ml-100k/ https://gitlab.com/vuthanho/data/-/raw/main/ml-100k/data_te.mat