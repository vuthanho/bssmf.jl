using Statistics
using DelimitedFiles

# Custom packages
using BSSMF

dataset = "ml-1m"
println("number of threads : $(Threads.nthreads())")
path_tr = pwd()*"/xp/"*dataset*"/data_tr.mat"
path_te = pwd()*"/xp/"*dataset*"/data_te.mat"

rs = [1,5,10,20,50,100]
runs = 10
models = ["BSSMF","NMF","MF"]
rmses = Array{Float64}(undef,runs,length(rs),length(models))
for run in 1:runs
    println("run $run/$runs")
    println("")
    for k in eachindex(rs)
        r=rs[k]
        for (model_k,model) in enumerate(models)
            # Loading data
            wrkspc = Workspace(path_tr,r)
            # vad_wrkspc = Workspace(path_vad,r)
            te_wrkspc = Workspace(path_te,r)
            # Settings according to the model
            if model=="BSSMF"
                meanX = mean(wrkspc.V)
                wrkspc.V .-= meanX
                # vad_wrkspc.V .-= meanX
                # vad_wrkspc.Vtest .-= meanX
                te_wrkspc.V .-= meanX
                te_wrkspc.Vtest .-= meanX
                simplex_proj=true
                minW=NaN # NaN to automatically choose minimum(wrkspc.V)
                maxW=NaN # NaN to automatically choose maximum(wrkspc.V)
            elseif model=="NMF"
                simplex_proj=false
                minW=0
                maxW=Inf
            elseif model=="MF"
                simplex_proj=false
                minW=-Inf
                maxW=Inf
            end
            println("run : $run | r : $r | model : $model")
            @time err,err_time = bssmf!(wrkspc,maxiter=200,verbose=true,minW=minW,maxW=maxW,simplex_proj=simplex_proj,inneriter=1);
            te_wrkspc.W = wrkspc.W
            # Predicting on test set
            res = pred!(te_wrkspc,simplex_proj=simplex_proj,inneriter=200)
            println("RMSE train : $(BSSMF.rmse(wrkspc))")
            # println("RMSE vad : $(vad_wrkspc.rmsebest)")
            println("RMSE test : $res")

            # Saving results
            rmses[run,k,model_k] = res
        end
    end
end

# Displaying results
for (model_k,model) in enumerate(models)
    print("mean $model :")
    display(mean(rmses[:,:,model_k],dims=1))
    print("std $model :")
    display(std(rmses[:,:,model_k],dims=1))
end

# Displaying results
for (model_k,model) in enumerate(models)
    println()
    mean_k = round.(mean(rmses[:,:,model_k],dims=1);digits=2)
    std_k = round.(std(rmses[:,:,model_k],dims=1);sigdigits=1)
    print("$model & ")
    for k_r in eachindex(rs)
        if k_r == last(eachindex(rs))
            print("\$$(mean_k[k_r]) \\pm $(std_k[k_r])\$ \\\\")
        else
            print("\$$(mean_k[k_r]) \\pm $(std_k[k_r])\$ & ")
        end
    end
end