# BSSMF.jl

Bounded Simplex Structured Matrix Factorization in Julia. This work is associated with this [article](https://doi.org/10.48550/arXiv.2209.12638). There is also a shorter [version](https://ieeexplore.ieee.org/abstract/document/9747124) (whose preprint is available on [research gate](https://www.researchgate.net/publication/355043833_Bounded_Simplex-Structured_Matrix_Factorization)).

This code in Julia allows you to solve the BSSMF optimization problem: 
Given a matrix $X\in\mathbb{R}^{m\times n}$, a factorization rank $r$, and an interval $[a,b]$ in $\mathbb{R}^m$, 
solve
```math
\begin{aligned}
       \min_{W,H} &||X-WH||_F^2 \\
       \text{s.t. } &a \leq W \leq b,\\
       & H \geq 0, H^\top e=e, 
\end{aligned}
```
where $e$ is the vector of all ones of appropriate dimension and the inequalities constraints for $W$ and $H$ are element-wise.

# Option 1: Installing and using the package

## Recommended installation

* Start Julia
* Type `]` to enter `Pkg` mode
* Add my registry: `registry add https://gitlab.com/vuthanho/vuthanhoregistry`
* Install the BSSMF package: `add BSSMF`
* Leave `Pkg` mode with backspace ⌫
* `using BSSMF`

## Fast installation that might break in the future

* Start Julia
* Type `]` to enter `Pkg` mode
* Install the BSSMF package: `add https://gitlab.com/vuthanho/bssmf.jl`
* Leave `Pkg` mode with backspace ⌫
* `using BSSMF`

## Example

```julia
using BSSMF
using LinearAlgebra

# Minimal example

# Genarating some synthetic data
W = 4*[2 3 0
       3 2 0
       3 0 2
       2 0 3
       0 2 3
       0 3 2]
H = 1/4*[1 3 3 1 0 0
         3 1 0 0 1 3
         0 0 1 3 3 1]
X = W*H # Unique bounded simplex structured matrix factorization with min = 0 and max = 12
r = size(H,1) # Rank of the factorization
# Loading the data into a Workspace structure
wrkspc = Workspace(X,r)

# Solving BSSMF and retrieving the rmse according to time as an output
err,err_time = bssmf!(wrkspc)
display(err[end]) # Displaying the final reconstruction error
display(wrkspc.W) # Displaying the estimated W
display(wrkspc.H) # Displaying the estimated H
```
Here the reconstruction error is bad because by default `bssmf!` chooses the output of `extrema(X)` for the bounds, which would be `[2.0 11.0]` in this example.
With the right bounds, we can retrieve the true and unique factors `W` and `H` :
```julia
wrkspc = Workspace(X,r)
err,err_time = bssmf!(wrkspc,minW=0,maxW=12)
display(err[end])
display(wrkspc.W)
display(wrkspc.H)
```
## Parameters
The default parameters for `bssmf!` are :
*    `inneriter = 10`: Number of times one factor is updated in a row
*    `maxtime = Inf`: Maximum allotted time (Doesn't take into account the time taken to compute the error)
*    `maxiter = 500`: Maximum number of outer iterations, equivalent to the number of epoch (doesn't take into account `inneriter`)
*    `verbose = false`: Option to add verbose
*    `simplex_proj = true`: Option to disable the projection onto the probability simplex
*    `minW = minimum(X)`: Minimum bound
*    `maxW = maximum(X)`: Maximum bound
*    `extra = true`: Option to disable the TITAN extrapolation sequence

## Initialization

You can initialize a `Workspace` with the matrices `W` and `H` of your choice: `wrkspc = Workspace(X,r,W=Winit,H=Hinit)`. Note that `Winit` and `Hinit` will be modified if you call `bssmf!` because `wrkspc.W` is not a hard copy of `Winit`, just another alias. If `W` and\or `H` are not specified, they will be randomly generated within their respective feasible set when calling `bssmf!`.

## Working with missing data

`BSSMF.jl` also handles missing data if `Workspace` is given a sparse matrix `X`. Here is a minimal example:
```julia
using BSSMF
using LinearAlgebra
using SparseArrays
using Random
# Minimal example with missing data

# Genarating some synthetic data
W = 4*[2 3 0
       3 2 0
       3 0 2
       2 0 3
       0 2 3
       0 3 2]
H = 1/4*[1 3 3 1 0 0
         3 1 0 0 1 3
         0 0 1 3 3 1]
s = [trues(4);falses(2)][randperm(6)]
shift = randperm(6)
# Generating a boolean matrix to select 4 out 6 elements per row and per column
M = reduce(hcat,[circshift(s,shift[i]) for i in 1:6])
X = sparse((W*H).*M) # X is now a sparse matrix. Note that by using sparse(.) like that, zeros are considered as missing values. Depending on how you implemented X, it is possible to consider some zeros as known values. For instance, using sparse(I,J,V,m,n) will not drop the zeros in V.
r = size(H,1)
# Loading the data into a Sparse Workspace structure.
wrkspc = Workspace(X,r)

# Solving BSSMF and retrieving the rmse on the know values according to time as an output
err,err_time = bssmf!(wrkspc,minW=0,maxW=12)
display(err[end]) # Displaying the final reconstruction error
display(wrkspc.W) # Displaying the estimated W
display(wrkspc.H) # Displaying the estimated H
``` 
### Some remarks
* Note that, if we call `M` the boolean matrix of know values, it is not possible to save some computation by precomputing `H*H'`, `X*H'`, `W'W` and `W'X` like when the full matrix `X` is known. This is because the gradients relatively to `W` and `H` are respectively `W'*(M.*(W*H-X))` and `(M.*(W*H-X))*H'`. The Hadamard product `.*` makes it impossible to precompute some matrix products. Hence, using `inneriter=1` is not particularly a bad choice. 
* The operation `M.*(W*H-X)` is computed by taking advantage of the sparsity and of the parallelization. Starting julia with multiple threads, with `julia -t 6` for instance, will greatly improve the computation time of `bssmf!` on data with missing values.
* A little subtlety, when working with a sparse Workspace `wrkspc`, `wrkspc.W` is in fact a hard copy of the transpose of `W`. The reason is that when we need to compute `M.*(W*H-X)`, a lot of access to `W[i,:]` are done. However, julia is faster when accessing to columns than to rows. Hence, instead of reading a row, we work on a hard copy of the transpose of `W` that we will call here `Wt`. Then, reading a column of `Wt` is just equivalent to reading a row of `W`. For this reason, if you provide an initialization of `W` to a sparse workspace, solving this workspace will not change the given initialized `W`, as a hard copy is made.

# Option 2: Running the experiments from our [paper](https://doi.org/10.48550/arXiv.2209.12638)
This is necessary only to reproduce our experiments.

* Clone this repository with `git clone https://gitlab.com/vuthanho/bssmf.jl`
* Download the datasets. If you have a Unix-like system, the datasets can be easily downloaded by running the script [download_data.sh](/xp/download_data.sh) **while** in the folder `xp`. If not, you can download them from this [repository](https://gitlab.com/vuthanho/data). The structure should be the following:
```
.
├── LICENSE
├── LocalPreferences.toml
├── Manifest.toml
├── Project.toml
├── README.md
├── src
└── xp
    ├── convergence_speed.jl
    ├── download_data.sh
    ├── ml-100k
    │   ├── data_te.mat
    │   └── data_tr.mat
    ├── ml-100k_run.jl
    ├── ml-1m
    │   ├── data_te.mat
    │   └── data_tr.mat
    ├── ml-1m_run.jl
    └── mnist.mat
```

* Enter the project folder (`cd bssmf.jl`) 
* Start julia (julia can be started with multiple threads with `julia -t n` where `n` is the number of threads )
* Type `]` to enter Pkg mode
* Type `activate .`
* Type `instantiate` to install the necessary package for BSSMF.jl
* Install [PlotlyLight](https://github.com/JuliaComputing/PlotlyLight.jl) (only needed to make the plots from `convergence_speed.jl`) by typing `add PlotlyLight`
* Type backspace to leave Pkg mode
* The Figure 2 can be computed by running `include("xp/convergence_speed.jl")`
* The Table 1 can be computed by running `include("xp/ml-1m_run.jl")`
* The Table 2 can be computed by running `include("xp/ml-100k_run.jl")`
# License
BSSMF.jl is free software, licensed under the [GNU GPL v3](http://www.gnu.org/licenses/gpl.html).
