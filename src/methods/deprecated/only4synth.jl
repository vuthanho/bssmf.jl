# Algorithms for synthetic_run

# #Dependencies :
# using SparseArrays
# using LinearAlgebra

export bssmf,nmf

# BSSMF for Sparse Matrices
function bssmf( X::AbstractSparseMatrix{T},
                Xfull::AbstractMatrix{T},
                r::Integer,
                W0::AbstractMatrix{Float64}=Matrix{Float64}(undef,0,0),
                H0::AbstractMatrix{Float64}=Matrix{Float64}(undef,0,0),
                options::Dict{String,Number}= Dict{String,Number}()) where T <: AbstractFloat

    m,n = size(X)
    Diff = Float64.(X) # Will contain (X-WH)[coord]
    I,J,V = findnz(Diff)
    coord = CartesianIndex.(I,J)
    
    # Loading parameters
    default_options!(options,X[coord])
    inneriter   = options["inneriter"]
    delta_iter  = options["delta_iter"]
    maxtime     = options["maxtime"]
    mina        = options["mina"]
    maxb        = options["maxb"]
    delta_time  = options["delta_time"]

    # Initializing
    W0 = length(W0) == 0 ? mina.+(maxb-mina)*rand(m, r) : W0
    # W0 = length(W0) == 0 ? rand(m, r) : W0
    W = copy(W0)
    H0 = length(H0) == 0 ? rand(r, n) : H0
    H = condatProj(H0)

    LpW = opnorm(H*H')
    LW = LpW
    LpH = opnorm(W'W)
    k = 0
    Wold = copy(W)
    Hold = copy(H)
    alpha1 = 1.0
    alpha2 = 1.0

    diff = view(Diff,coord) # This is the only part of Diff that will be updated
    partXmWH!(diff,V,W,H,I,J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err = NaN.*Vector{Float64}(undef,ceil(Int,maxtime/delta_time)+1)
    RMSE_train = copy(err)
    RMSE_test = copy(err)
    err_time = copy(err)
    err_index = 1
    err_time[err_index] = 0.0
    err[err_index] = 0.5*norm(diff)^2
    RMSE_train[err_index] = sqrt(1/length(coord)*sum(diff.^2))
    RMSE_test[err_index] = sqrt(1/(m*n-length(coord))*sum((Xfull-W*H-Diff).^2))
    ety0 = 0.0

    println("Starting BSSMF")
    t0 = time()
    while err_time[err_index] <= maxtime
        k += 1
        print("$k ... ")

        # Update W
        iter = 1
        while iter<= inneriter
            alpha0 = alpha1
            alpha1 = 0.5*(1+sqrt(1+4*alpha0^2))
            beta = min((alpha0-1)/alpha1 , 0.9999*sqrt(LpW/LW))
            Wextra = W + beta*(W-Wold)
            # Update Diff by updating diff
            partXmWH!(diff,V,Wextra,H,I,J)
            Wold = W
            W = Wextra+1/LW*(Diff*H')
            setminmax!(W,mina,maxb)
            LpW = LW
            iter+=1
        end
        LH = opnorm(W'*W)

        # Update H
        iter = 1
        while iter <= inneriter
            alpha0 = alpha2
            alpha2 = 0.5*(1+sqrt(1+4*alpha0^2))
            beta = min((alpha0-1)/alpha2 , 0.9999*sqrt(LpH/LH))
            Hextra = H + beta*(H-Hold)
            # Update Diff by updating diff
            partXmWH!(diff,V,W,Hextra,I,J)
            Hold=H
            H = condatProj( Hextra + 1/LH*(W'*Diff), 0.0 )
            # l1ballproj!(H, 0.0)
            LpH=LH

            iter = iter + 1
        end
        LW = opnorm(H*H')

        # Saving the error each delta_time seconds
        if (time()-t0) - ety0 >= delta_time || err_time[err_index] + (time()-t0) - ety0 > maxtime
            etz = time()-t0
            err_index +=1
            err[err_index] = 0.5*norm(diff)^2
            RMSE_train[err_index] = sqrt(1/length(coord)*sum(diff.^2))
            RMSE_test[err_index] = sqrt(1/(m*n-length(coord))*sum((Xfull-W*H-Diff).^2))
            err_time[err_index] = err_time[err_index-1] + etz - ety0
            ety0 = time()-t0
        end
    end

    # println("Number of iteration: $k")
    return W,H,err,RMSE_train,RMSE_test,err_time
end

# NMF for Sparse Matrices
function nmf(   X::AbstractSparseMatrix{T},
                Xfull::AbstractMatrix{T},
                r::Integer,
                W0::AbstractMatrix{Float64}=Matrix{Float64}(undef,0,0),
                H0::AbstractMatrix{Float64}=Matrix{Float64}(undef,0,0),
                options::Dict{String,Number}= Dict{String,Number}()) where T <: AbstractFloat

    m,n = size(X)
    Diff = Float64.(X) # Will contain (X-WH)[coord]
    I,J,V = findnz(Diff)
    coord = CartesianIndex.(I,J)
    
    # Loading parameters
    default_options!(options,X[coord])
    inneriter   = options["inneriter"]
    delta_iter  = options["delta_iter"]
    maxtime     = options["maxtime"]
    mina        = options["mina"]
    maxb        = options["maxb"]
    delta_time  = options["delta_time"]

    # Initializing
    W0 = length(W0) == 0 ? rand(m, r) : W0
    # W0 = length(W0) == 0 ? rand(m, r) : W0
    W = copy(W0)
    H0 = length(H0) == 0 ? rand(r, n) : H0
    H = condatProj(H0)

    LpW = opnorm(H*H')
    LW = LpW
    LpH = opnorm(W'W)
    k = 0
    Wold = copy(W)
    Hold = copy(H)
    alpha1 = 1.0
    alpha2 = 1.0

    diff = view(Diff,coord) # This is the only part of Diff that will be updated
    partXmWH!(diff,V,W,H,I,J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err = NaN.*Vector{Float64}(undef,ceil(Int,maxtime/delta_time)+1)
    RMSE_train = copy(err)
    RMSE_test = copy(err)
    err_time = copy(err)
    err_index = 1
    err_time[err_index] = 0.0
    err[err_index] = 0.5*norm(diff)^2
    RMSE_train[err_index] = sqrt(1/length(coord)*sum(diff.^2))
    RMSE_test[err_index] = sqrt(1/(m*n-length(coord))*sum((Xfull-W*H-Diff).^2))
    ety0 = 0.0

    println("Starting NMF")
    t0 = time()
    while err_time[err_index] <= maxtime
        k += 1
        print("$k ... ")

        # Update W
        iter = 1
        while iter<= inneriter
            alpha0 = alpha1
            alpha1 = 0.5*(1+sqrt(1+4*alpha0^2))
            beta = min((alpha0-1)/alpha1 , 0.9999*sqrt(LpW/LW))
            Wextra = W + beta*(W-Wold)
            # Update Diff by updating diff
            partXmWH!(diff,V,Wextra,H,I,J)
            Wold = W
            W = max.(Wextra+1/LW*(Diff*H'),0.0)
            LpW = LW
            iter+=1
        end
        LH = opnorm(W'*W)

        # Update H
        iter = 1
        while iter <= inneriter
            alpha0 = alpha2
            alpha2 = 0.5*(1+sqrt(1+4*alpha0^2))
            beta = min((alpha0-1)/alpha2 , 0.9999*sqrt(LpH/LH))
            Hextra = H + beta*(H-Hold)
            # Update Diff by updating diff
            partXmWH!(diff,V,W,Hextra,I,J)
            Hold=H
            H = max.( Hextra + 1/LH*(W'*Diff), 0.0 )
            LpH=LH

            iter = iter + 1
        end
        LW = opnorm(H*H')

        # Saving the error each delta_time seconds
        if (time()-t0) - ety0 >= delta_time || err_time[err_index] + (time()-t0) - ety0 > maxtime
            etz = time()-t0
            err_index +=1
            err[err_index] = 0.5*norm(diff)^2
            RMSE_train[err_index] = sqrt(1/length(coord)*sum(diff.^2))
            RMSE_test[err_index] = sqrt(1/(m*n-length(coord))*sum((Xfull-W*H-Diff).^2))
            err_time[err_index] = err_time[err_index-1] + etz - ety0
            ety0 = time()-t0
        end
    end

    # println("Number of iteration: $k")
    return W,H,err,RMSE_train,RMSE_test,err_time
end

function setminmax!(y::AbstractMatrix{T},a::Number,b::Number) where T <: AbstractFloat
    m,n = size(y)
    Threads.@threads for i in 1:m
        for j in 1:n
            y[i,j] = min(max(y[i,j] , a) , b)
        end
    end
end

function setmax!(y::AbstractMatrix{T},a::Number) where T <: AbstractFloat
    m,n = size(y)
    Threads.@threads for j in 1:n
        for i in 1:m
            y[i,j] = max(y[i,j],a)
        end
    end
end

function default_options!(options,X)
    if !haskey(options,"inneriter")
        options["inneriter"]=20
    end
    if !haskey(options,"delta_iter")
        options["delta_iter"] = 1e-3
    end
    if !haskey(options,"maxtime")
        options["maxtime"] = 5
    end
    if !haskey(options,"mina")
        options["mina"] = minimum(X)
    end
    if !haskey(options,"maxb")
        options["maxb"] = maximum(X)
    end
    if !haskey(options,"delta_time")
        options["delta_time"] = 1
    end
end


function partXmWH!( diff::SubArray{Float64},
    V::AbstractVector{T},
    W::AbstractMatrix{Float64},
    H::AbstractMatrix{Float64},
    I::Vector{Int64},
    J::Vector{Int64}) where T<:AbstractFloat

    Threads.@threads for k in 1:length(I)
        diff[k]= V[k] - W[I[k],:]'*H[:,J[k]]
    end
    return nothing
end