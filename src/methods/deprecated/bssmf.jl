# Algorithm for BSSMF

# #Dependencies :
using SparseArrays
using LinearAlgebra

include("../common/snpa.jl")
include("../common/powermethod.jl")
include("../common/lpProjs.jl")

export bssmf

# BSSMF for Sparse Matrices
function bssmf( X::AbstractSparseMatrix{T},
                r::Integer;
                Wt::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                H::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                options::Dict{String,Number}=Dict{String,Number}(),
                train_size::Union{AbstractFloat,Nothing}=nothing) where T <: AbstractFloat

    m, n = size(X)
    Diff = X # Will contain (X-WH)[coord]
    X = nothing
    GC.gc()
    I, J, V = findnz(Diff)
    training = ~isnothing(train_size)
    if training
        length_train_set = floor(Int, length(V) * train_size)
        length_test_set = length(V) - length_train_set
        idx_train_set = shuffle(1:length(V))
        idx_test_set = idx_train_set[1:length_test_set]
        idx_train_set = idx_train_set[length_test_set + 1:end]
        idx_train_set = idx_train_set[sortperm(idx_train_set)]
        @assert length(idx_train_set) == length_train_set
        Itest, Jtest, Vtest = I[idx_test_set], J[idx_test_set], V[idx_test_set]
        I, J, V = I[idx_train_set], J[idx_train_set], V[idx_train_set]
        Diff = sparse(I, J, V, m, n)
    end
    # Loading parameters
    default_options_bssmf!(options)
    inneriter   = options["inneriter"]
    delta_iter  = options["delta_iter"]
    maxtime     = options["maxtime"]
    maxiter     = options["maxiter"]
    @assert maxiter!=Inf
    if !haskey(options, "mina")
        mina = minimum(V)
    else
        mina = options["mina"]
    end
    if !haskey(options, "maxb")
        maxb = maximum(V)
    else
        maxb = options["maxb"]
    end
    # Initializing
    print("Initializing W and H ... ")
    Wt = length(Wt) == 0 ? T.(powermethod(Diff, randn(T, n, r))') : Wt
    minWt = minimum(Wt)
    maxWt = maximum(Wt)
    Wt = mina .+ (maxb - mina) / (maxWt - minWt) * (Wt .- minWt)
    maxWt = Nothing()
    minWt = Nothing()
    H = length(H) == 0 ? T.(svd(Wt * Diff).V') : H
    println("✓")
    # W0 = length(W0) == 0 ? rand(m, r) : W0
    # H = length(H) == 0 ? rand(T, r, n) : H
    # H = condatProj(H)

    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(Wt * Wt')
    k = 1
    Wtold = copy(Wt)
    Hold = copy(H)
    alpha1 = T(1)
    alpha2 = T(1)

    diff = zeros(T, length(V))
    partXmWH!(diff, V, Wt, H, I, J) # Computing (X-WH)[coord] and storing it in Diff
    
    # Error initialization
    err_time = NaN .* Vector{Float64}(undef, maxiter+1)
    err_time[k] = 0.0
    if training
        diff_test = zeros(T, length_test_set)
        partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
        rmse_train = copy(err_time)
        rmse_test = copy(err_time)
        rmse_train[k] = sqrt(1 / length_train_set * sum(diff.^2))
        rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test.^2))
    else
        err = copy(err_time)
        err[k] = 0.5 * norm(diff)^2
    end
    ety0 = 0.0

    println("Starting BSSMF (sparse)")
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime
        display_progress(k,maxiter,err_time[k],maxtime)
        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wtextra = Wt + beta * (Wt - Wtold)
            # Update Diff
            @time partXmWH!(Diff, V, Wtextra, H, I, J)
            Wtold = Wt
            Wt = Wtextra + 1 / LW * (H * Diff')
            setminmax!(Wt, mina, maxb)
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end
        LH = opnorm(Wt * Wt')
        # LH = opnorm(Wt)^2

        # Update H
        iter = 1
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            # Update Diff
            partXmWH!(Diff, V, Wt, Hextra, I, J)
            Hold = H
            H = condatProj(Hextra + 1 / LH * (Wt * Diff), T(1e-16))
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        LW = opnorm(H * H')
        # LW = opnorm(H)^2

        # Saving the error

        etz = time() - t0
        k += 1
        if training
            partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
            rmse_train[k] = sqrt(1 / length_train_set * sum(Diff.nzval.^2))
            rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test.^2))
        else
            err[k] = 0.5 * norm(diff)^2
        end
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end
        
    # println("Number of iteration: $k")
    inan = findfirst(isnan,err_time)
    if training
        return Wt', H, rmse_train[1:(isnothing(inan) ? end : inan-1)], rmse_test[1:(isnothing(inan) ? end : inan-1)], err_time[1:(isnothing(inan) ? end : inan-1)]
    else
        return Wt', H, err[1:(isnothing(inan) ? end : inan-1)], err_time[1:(isnothing(inan) ? end : inan-1)]
    end
    
end

# BSSMF for Dense Matrices
function bssmf( X::AbstractMatrix{T},
                r::Integer;
                W::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                H::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                options::Dict{String,Number}=Dict{String,Number}()) where T <: AbstractFloat

    m, n = size(X)
    normX2 = norm(X)^2

    # Loading parameters
    default_options_bssmf!(options)
    inneriter   = options["inneriter"]
    delta_iter  = options["delta_iter"]
    maxtime     = options["maxtime"]
    maxiter     = options["maxiter"]
    @assert maxiter!=Inf
    if !haskey(options, "mina")
        mina = minimum(X)
    else
        mina = options["mina"]
    end
    if !haskey(options, "maxb")
        maxb = maximum(X)
    else
        maxb = options["maxb"]
    end
    # Initializing
    print("Initializing W and H ... ")
    W = length(W) == 0 ? rand(T,m , r) : W
    minW = minimum(W)
    maxW = maximum(W)
    W = mina .+ (maxb - mina) / (maxW - minW) * (W .- minW)
    H = length(H) == 0 ? rand(T, r, n) : H
    H = condatProj(H)
    println("✓")

    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(W' * W)
    k = 1
    Wold = copy(W)
    Hold = copy(H)
    HHt = H * H'
    XHt = X * H'
    alpha1 = T(1)
    alpha2 = T(1)
    
    # Error initialization
    err = NaN .* Vector{Float64}(undef, maxiter+1)
    err_time = copy(err)
    err_time[k] = 0.0
    err[k] = 0.5 * norm(X - W * H)^2
    ety0 = 0.0

    println("Starting BSSMF (dense)")
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime
        display_progress(k,maxiter,err_time[k],maxtime)
        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wextra = W + beta * (W - Wold)
            Wold = W
            W = Wextra + 1 / LW * (XHt - Wextra * HHt)
            setminmax!(W, mina, maxb)
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end

        # Update H
        iter = 1
        WtW = W' * W
        WtX = W' * X
        LH = opnorm(WtW)
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            Hold = H
            H = condatProj(Hextra + 1 / LH * (WtX - WtW * Hextra), zero(T))
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        
        HHt = H * H'
        XHt = X * H'
        LW = opnorm(HHt)

        # Saving the error
        etz = time() - t0
        k += 1
        err[k] = 0.5 * (normX2 - 2 * sum(W .* XHt) + sum(WtW .* HHt))
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end
        
    # println("Number of iteration: $k")
    inan = findfirst(isnan,err_time)
    return W, H, err[1:(isnothing(inan) ? end : inan-1)], err_time[1:(isnothing(inan) ? end : inan-1)]
end

function setminmax!(y::AbstractMatrix{T}, a::Number, b::Number) where T <: AbstractFloat
    m, n = size(y)
    if Threads.nthreads() > 1
        @views Threads.@threads for i in 1:m
            for j in 1:n
                y[i,j] = min(max(y[i,j], a), b)
            end
        end
    else
        @views for i in 1:m
            for j in 1:n
                y[i,j] = min(max(y[i,j], a), b)
            end
        end
    end
end

function setmax!(y::AbstractMatrix{T}, a::Number) where T <: AbstractFloat
    m, n = size(y)
    if Threads.nthreads() > 1
        Threads.@threads for j in 1:n
            for i in 1:m
                y[i,j] = max(y[i,j], a)
            end
        end
    else
        for j in 1:n
            for i in 1:m
                y[i,j] = max(y[i,j], a)
            end
        end
    end
end
    
function default_options_bssmf!(options)
    if !haskey(options, "inneriter")
        options["inneriter"] = 10
        end
    if !haskey(options, "delta_iter")
        options["delta_iter"] = 1e-3
    end
    if !haskey(options, "maxtime")
        options["maxtime"] = 5
    end
    if !haskey(options, "maxiter")
        options["maxiter"] = 500
    end
end

function partXmWH!( diff::SubArray{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat
    
    if Threads.nthreads() > 1
        Threads.@threads for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else
        for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function partXmWH!( diff::AbstractVector{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat
    
    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else
        
        @views for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function partXmWH!( Diff::AbstractSparseMatrix{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat

    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            Diff.nzval[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else
        
        @views for k in 1:length(I)
            Diff.nzval[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function setSval!( Diff::AbstractSparseMatrix{T},
    diff::AbstractVector{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat

    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            Diff[I[k],J[k]] = diff[k]
        end
    else
        @views for k in 1:length(I)
            Diff[I[k],J[k]] = diff[k]
        end
    end
end

function display_progress(k,maxiter,t,maxtime)
    print(" $(ceil(Int64,100*max(k/maxiter,t/maxtime)))%\r")
end