export titanpsnmf

# This can be speeded up with Threads.@threads

function titanpsnmf(X,W,Ht,options)
    if haskey(options,"inneriter")
        inneriter = options["inneriter"]
    else
        inneriter::UInt16 = 10
    end
    if haskey(options,"delta_iter")
        delta_iter = options["delta_iter"]
    else
        delta_iter::Float64 = 1e-6
    end
    if haskey(options,"sparsity")
        s = options["sparsity"]
    else
        s::Float64 = 0.25
    end
    maxtime = options["maxtime"]
    r = size(W,2)
    # Optimal scaling
    alpha = sum(sum(W'*X.*Ht',dims=1))/sum(sum((W'*W).*(Ht'*Ht),dims=1))
    W = alpha*W
    # Projection of H to achieve average sparsity s 
    Ht = weightedgroupedsparseproj(Ht,s)
    # Pre-computing and initializing some variables
    WtW::Matrix{Float64} = W'*W
    XtW::Matrix{Float64} = X'*W
    HHt::Matrix{Float64} = Ht'*Ht
    XHt::Matrix{Float64} = X*Ht
    LpW::Float64 = opnorm(HHt)
    LW = LpW
    LpH::Float64 = opnorm(WtW)
    k::Int16 = 1
    Wold::Matrix{Float64} = W
    Htold::Matrix{Float64} = Ht
    alpha1::Float64 = 1.0
    alpha2::Float64 = 0.1

    t0 = time()
    while time()-t0<maxtime
        k += 1
        # Normalize W and H so that columns/rows have the same norm, that is,
        # ||W(:,k)|| = ||H(k,:)|| for all k
        normW = sqrt.(sum(abs2,W ,dims=1)).+1e-16
        normH = sqrt.(sum(abs2,Ht,dims=1)).+1e-16
        for k in 1:r
            W[:,k]  ./= sqrt(normW[k])/sqrt(normH[k]) 
            Ht[:,k] ./= sqrt(normH[k])/sqrt(normW[k]) 
        end
        # Update W
        iter = 1
        eps0 = 0
        eps = 1
        Woldold = W
        while iter<= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha1
            alpha1 = 0.5*(1+sqrt(1.0+4*alpha0^2))
            beta = min((alpha0-1.0)/alpha1 , 0.9999*sqrt(LpW/LW))
            Wextra = W + beta*(W-Wold)
            Wold = W
            W = max.(Wextra + 1/LW*(XHt-Wextra*HHt),1e-16)
            if iter == 1
                eps0 = norm(W-Woldold)
            end
            LpW = LW
            eps = norm(W-Woldold)
            iter+=1
        end
        WtW = W'*W
        LH = opnorm(WtW)
        XtW = X'*W

        # Update Ht
        iter = 1
        eps0 = 0
        eps = 1
        # Htoldold = Ht
        while iter <= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha2
            alpha2 = 0.5*(1+sqrt(1.0+4*alpha0^2))
            beta = min((alpha0-1.0)/alpha2 , 0.9999*sqrt(LpH/LH))
            Htextra = Ht + beta*(Ht-Htold)
            Htold=Ht
            # Projection à adapter pour PSNMF
            Ht = weightedgroupedsparseproj(max.(Htextra + 1/LH*(XtW - Htextra*WtW) , 0),s)
            # if iter == 1
            #     eps0 = norm(Ht-Htoldold)
            # end
            # eps = norm(Ht-Htoldold)
            LpH=LH
            iter = iter + 1
        end
        HHt = Ht'*Ht
        LW = opnorm(HHt)
        XHt = X*Ht
    end
    println("Number of iteration: $k")
    return W,Ht'
end

# function sparseproj(x,s,options::Dict{String,Number}=Dict{String,Number}())
#     m,n = size(x)
#     L2 = norm.(eachcol(x))
#     L1 = (sqrt(n)-s*(sqrt(n)-1))*L2
#     x .+= (L1' .- sum(x,dims=1))./m
# end

function weightedgroupedsparseproj(x,s,options::Dict{String,Number}=Dict{String,Number}())

    m,n = size(x)
    if haskey(options,"w")
        w = options["w"]
        optionsw = true
    else
        optionsw = false
    end

    if haskey(options,"precision")
        prec = options["precision"]
    else
        prec = 1e-3
    end
    if haskey(options,"linrat")
        linrat = options["linrat"]
    else
        linrat = 0.9
    end
    if s<0 || s>1
        error("the sparsity parameter has to be in [0,1]")
    end
    # Replace x with |x|, keep the sign sw of its entries
    # and compute the parameter k
    k = 0.0
    muup0 = 0.0
    critmu = Vector{Float64}()
    if optionsw # With a custom weight
        for i in 1:n
            nwi = norm(w[:,i])
            betaim1 = nwi - minimum(w[:,i])
            k += nwi/betaim1
            # check critical values of mu where g(mu) is discontinuous, that is, 
            # where the two (or more) largest entries of x{i} are equal to one
            # another 
            critxi,maxxi = wcheckcrit(view(x,:,i),view(w,:,i))
            if critxi
                append!(critmu,maxxi*betaim1)
            end
            muup0 = max(muup0,maxxi*betaim1)
        end
    else # without a custom weight
        nwi = sqrt(m)
        betam1 = nwi-1
        k = n*nwi/betam1
        critx,maxx = wcheckcrit(view(x,:,:))
        jcritx = findall(critx)
        append!(critmu,betam1*maxx[jcritx])
        muup0 = betam1*maximum(maxx)
    end
    k -= n*s
    if optionsw
        vgmu,xnew,gradg = wgmu(view(x,:,:),view(w,:,:),0)
    else
        vgmu,xnew,gradg = wgmu(view(x,:,:),0)
    end
    if vgmu < k
        return x
    else
        numiter = 0
        mulow = 0
        glow = vgmu
        muup = muup0
        # Initialization on mu using 0, it seems to work the best because the
        # slope at zero is rather steep
        newmu = 0
        gnew = glow
        gpnew = gradg
        Delta = muup - mulow
        precn = prec*n
        while abs(gnew-k)>precn && numiter<100
            oldmu = newmu
            # Newton
            newmu = oldmu + (k-gnew)/gpnew
            if newmu>=muup || newmu<=mulow
                newmu = (mulow+muup)/2
            end
            if optionsw
                gnew,xnew,gpnew = wgmu(view(x,:,:),view(w,:,:),newmu)
            else
                gnew,xnew,gpnew = wgmu(view(x,:,:),newmu)
            end
            if gnew < k
                gup = gnew;
                xup = xnew;
                muup = newmu;
            else
                glow = gnew;
                mulow = xnew;
                mulow = newmu;
            end
            # Guarantees linear convergence
            if muup - mulow > linrat*Delta && abs(oldmu-newmu)<(1-linrat)*Delta
                newmu = (mulow+muup)/2
                if optionsw
                    gnew,xnew,gpnew = wgmu(view(x,:,:),view(w,:,:),newmu)
                else
                    gnew,xnew,gpnew = wgmu(view(x,:,:),newmu)
                end
                if gnew < k
                    gup = gnew;
                    xup = xnew;
                    muup = newmu;
                else
                    glow = gnew;
                    mulow = xnew;
                    mulow = newmu;
                end
                numiter+=1
            end
            numiter+=1
            # Detects discontinuity
            if !isempty(critmu) && abs(mulow-muup) < abs(newmu)*prec && minimum( abs.(newmu .- critmu) ) < prec*newmu
                print("WARNING: the objective function is discontinuous around mu*")
                break
            end
        end
        return [xnew[:,i]'*x[:,i] for i in 1:n]'.*xnew
    end
end

function wgmu(x,mu)
    vgmu = 0
    gradg = 0
    m,n = size(x)
    beta = 1/(sqrt(m)-1)
    xp = x .-mu*beta
    for i in 1:n
        indtp = findall(xp[:,i].>0)
        if !isempty(indtp)
            lcindtp = m-length(indtp)
            if lcindtp != 0
                kc = 0
                k = 1
                while kc != lcindtp
                    if k-kc>length(indtp)
                        xp[k:end,i] .= 0.0
                        break
                    end
                    if k != indtp[k-kc]
                        kc+=1
                        xp[k,i] = 0.0
                    end
                    k+=1
                end
            end
            f2 = norm(xp[:,i])
            sxpi = sum(xp[:,i])
            gradg += beta^2 * (sxpi^2*f2^(-3) - length(indtp)*f2^(-1))
            xp[:,i]./=f2
            vgmu+=beta*sxpi/f2
        else
            im = argmax(xp[:,i])
            xp[:,i]=zeros(m)
            xp[im,i]=1
            vgmu+=beta
        end
    end
    return vgmu, xp, gradg
end

# function wgmu(x,mu)
#     vgmu = 0
#     gradg = 0
#     m,n = size(x)
#     beta = 1/(sqrt(m)-1)
#     xp = x .-mu*beta
#     for i in 1:n
#         indtp = findall(xp[:,i].>0)
#         if !isempty(indtp)
#             xp[:,i]=max.(0,xp[:,i])
#             f2 = norm(xp[:,i])
#             gradg += beta^2 * (sum(xp[indtp,i])^2*f2^(-3) - length(indtp)*f2^(-1))
#             xp[:,i]./=f2
#             vgmu+=beta*sum(xp[:,i])
#         else
#             im = argmax(xp[:,i])
#             xp[:,i]=zeros(m)
#             xp[im,i]=1
#             vgmu+=beta
#         end
#     end
#     return vgmu, xp, gradg
# end

function wgmu(x,w,mu)
    vgmu = 0
    gradg = 0
    m,n = size(x)
    beta = 1 ./( norm.(eachcol(w)) - minimum.(eachcol(w)) )
    xp = x .-mu*beta'.*w
    for i in 1:n
        indtp = findall(xp[:,i].>0)
        if !isempty(indtp)
            xp[:,i]=max.(0,xp[:,i])
            f2 = norm(xp[:,i])
            gradg += beta[i]^2 * ((w[indtp,i]'*xp[indtp,i])^2*f2^(-3) - w[indtp,i]'*w[indtp,i]*f2^(-1))
            xp[:,i]./=f2
            vgmu+=beta[i]*sum(xp[:,i].*w[:,i])
        else
            im = argmax(xp[:,i])
            xp[:,i]=zeros(m)
            xp[im,i]=1.0
            vgmu+=beta[i]*w[im,i]
        end
    end
    return vgmu, xp, gradg
end

function wcheckcrit(x,w,prec::Float64=1e-6)
    indi = findall(w.>0)
    xiwi = x[indi]./w[indi]
    maxx = maximum(xiwi)
    crit = abs.(xiwi.-maxx).<prec
    iscrit = findfirst(crit)
    iscrit = findnext(crit,iscrit)
    if iscrit === nothing
        return false,maxx
    else
        return true,maxx
    end
end

function wcheckcrit(x,prec::Float64=1e-6)
    maxx = maximum(x,dims=1)
    iscrit = sum(abs.(x.-maxx).<prec,dims=1).>1
    return iscrit,maxx
end