export titanminvol

function titanminvol(X,W,H,options)

    # Initializing parameters
    if haskey(options,"lambda")
        lambda = options["lambda"]
    else
        lambda::Float64 = 0.001
    end
    if haskey(options,"delta")
        delta = options["delta"]
    else
        delta::Float64 = 0.01
    end
    if haskey(options,"inneriter")
        inneriter = options["inneriter"]
    else
        inneriter::UInt16 = 10
    end
    if haskey(options,"delta_iter")
        delta_iter = options["delta_iter"]
    else
        delta_iter::Float64 = 1e-6
    end
    maxtime = options["maxtime"]

    # Pre-computing and initializing some variables
    WtW::Matrix{Float64} = W'*W
    WtX::Matrix{Float64} = W'*X
    HHt::Matrix{Float64} = H*H'
    XHt::Matrix{Float64} = X*H'
    delta_eyer = delta*I
    P::Matrix{Float64} = inv(HHt+delta_eyer)
    LpW::Float64 = opnorm(HHt+lambda*P)
    LpH::Float64 = opnorm(WtW)
    k::Int16 = 1
    Wold::Matrix{Float64} = copy(W)
    Hold::Matrix{Float64} = copy(H)
    alpha1::Float64 = 1.0
    alpha2::Float64 = 1.0

    t0 = time()
    while time()-t0<maxtime
        k += 1
        # Update W
        iter = 1
        eps0 = 0
        eps = 1
        Woldold = W
        while iter<= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha1
            alpha1 = 0.5*(1+sqrt(1+4*alpha0^2))
            P = inv( WtW + delta_eyer)
            LW = opnorm(HHt+lambda*P)
            beta = min((alpha0-1)/alpha1 , 0.9999*sqrt(LpW/LW))
            Wextra = W + beta*(W-Wold)
            Wold = W
            W = condatProj(Wextra+1/LW*(XHt-Wextra*(HHt+lambda*P)),1e-16)
            if iter == 1
                eps0 = norm(W-Woldold)
            end
            WtW = W'*W
            LpW = LW
            eps = norm(W-Woldold)
            iter+=1
        end
        LH = opnorm(WtW)
        WtX = W'*X

        # Update H
        iter = 1
        eps0 = 0
        eps = 1
        Holdold = H
        while iter <= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha2
            alpha2 = 0.5*(1+sqrt(1+4*alpha0^2))
            beta = min((alpha0-1)/alpha2 , 0.9999*sqrt(LpH/LH))
            Hextra = H + beta*(H-Hold)
            Hold=H
            H = max.(Hextra + 1/LH*(WtX - WtW*Hextra) , 1e-16)
            if iter == 1
                eps0 = norm(H-Holdold)
            end
            eps = norm(H-Holdold)
            LpH=LH
            iter = iter + 1
        end
        HHt = H*H'
        XHt = X*H'
    end
    println("Number of iteration: $k")
    return W,H
end