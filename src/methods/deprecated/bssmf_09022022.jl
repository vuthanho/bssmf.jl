# Algorithm for BSSMF

# #Dependencies :
using SparseArrays
using LinearAlgebra
using Base: @kwdef
using Parameters
using Random: shuffle

using BSSMF

export bssmf, bssmf!, mcp, AbstractWorkspace, Workspace, init_workspace!

abstract type AbstractWorkspace end

@kwdef mutable struct Workspace{T<:AbstractFloat} <: AbstractWorkspace where T
    X::AbstractMatrix{T}
    r::Int
    m::Int
    n::Int
    W::Matrix{T}=Matrix{T}(undef, 0, 0)
    H::Matrix{T}=Matrix{T}(undef, 0, 0)
    Wold::Matrix{T}=Matrix{T}(undef, 0, 0)
    Wextra::Matrix{T}=Matrix{T}(undef, 0, 0)
    Hold::Matrix{T}=Matrix{T}(undef, 0, 0)
    Hextra::Matrix{T}=Matrix{T}(undef, 0, 0)
    alpha1::T=T(1)
    alpha2::T=T(1)
    LW::T=T(1)
    LH::T=T(1)
    LpW::T=T(1)
    LpH::T=T(1)
    XHt::Matrix{T}=Matrix{T}(undef, 0, 0)
    HHt::Matrix{T}=Matrix{T}(undef, 0, 0)
    WtX::Matrix{T}=Matrix{T}(undef, 0, 0)
    WtW::Matrix{T}=Matrix{T}(undef, 0, 0)
end

function Workspace(X::AbstractMatrix{T},r::Int;kws...) where T
    m,n=size(X)
    return Workspace{T}(X=X,r=r,m=m,n=n,kws...)
end

function init_workspace!(wrkspc::Workspace{T},verbose=false,minW::T=-Inf,maxW::T=Inf) where T
    # Initializing
    m, n, r = wrkspc.m, wrkspc.n, wrkspc.r
    if verbose && (length(wrkspc.W) == 0 || length(wrkspc.H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(wrkspc.W) == 0
        wrkspc.W = rand(T, m, r)
        if !isinf(minW) && !isinf(maxW)
            oldminW = minimum(wrkspc.W)
            oldmaxW = maximum(wrkspc.W)
            if oldmaxW != oldminW
                wrkspc.W = minW .+ (maxW - minW) / (oldmaxW - oldminW) * (wrkspc.W .- oldminW)
            end
        end
    end

    # Init for H
    if length(wrkspc.H) == 0
        wrkspc.H = rand(T, r, n)
    end

    wrkspc.LpW = opnorm(wrkspc.H * wrkspc.H')
    wrkspc.LW = wrkspc.LpW
    wrkspc.LpH = opnorm(wrkspc.W' * wrkspc.W)
    wrkspc.LH = wrkspc.LpH
    wrkspc.Wold = copy(wrkspc.W)
    wrkspc.Hold = copy(wrkspc.H)
    wrkspc.Wextra = Matrix{T}(undef,m,r)
    wrkspc.Hextra = Matrix{T}(undef,r,n)
    wrkspc.HHt = wrkspc.H * wrkspc.H'
    wrkspc.XHt = wrkspc.X * wrkspc.H'
    wrkspc.WtW = wrkspc.W'* wrkspc.W
    wrkspc.WtX = wrkspc.W'* wrkspc.X
end

function updateW!(wrkspc::Workspace{T};
        inneriter=1,
        minW=-Inf,
        maxW=Inf,
        addzerocol=false) where T

    iter = 1
    while iter <= inneriter
        alpha0 = wrkspc.alpha1
        wrkspc.alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha1, T(0.9999) * sqrt(wrkspc.LpW / wrkspc.LW))
        # wrkspc.Wextra = wrkspc.W + beta .* (wrkspc.W - wrkspc.Wold)
        wrkspc.Wextra .= wrkspc.W
        wrkspc.Wextra .*= 1+beta
        wrkspc.Wextra .-= beta .*wrkspc.Wold
        wrkspc.Wold .= wrkspc.W
        # wrkspc.W = wrkspc.Wextra + 1 / wrkspc.LW * (wrkspc.XHt - wrkspc.Wextra * wrkspc.HHt)
        mul!(wrkspc.W,wrkspc.Wextra,wrkspc.HHt)
        wrkspc.W .*= -1
        wrkspc.W .+= wrkspc.XHt
        wrkspc.W ./= wrkspc.LW
        wrkspc.W .+= wrkspc.Wextra
        BSSMF.setminmax!(wrkspc.W, minW, maxW)
        if addzerocol wrkspc.W[:, end] .= zero(T) end
        wrkspc.LpW = wrkspc.LW
        iter += 1
    end
    mul!(wrkspc.WtW,wrkspc.W',wrkspc.W)
    # The following operations are the only ones that are allocating some memory
    mul!(wrkspc.WtX,wrkspc.W',wrkspc.X)
    wrkspc.LH = opnorm(wrkspc.WtW)
    return nothing
end

function updateH!(wrkspc::Workspace{T};
        inneriter=1,
        simplex_proj=true) where T

    iter = 1
    while iter <= inneriter # && eps_iter > delta_iter*eps0 
        alpha0 = wrkspc.alpha2
        wrkspc.alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha2, T(0.9999) * sqrt(wrkspc.LpH / wrkspc.LH))
        # wrkspc.Hextra = wrkspc.H + beta * (wrkspc.H - wrkspc.Hold)
        wrkspc.Hextra .= wrkspc.H
        wrkspc.Hextra .*= 1+beta
        wrkspc.Hextra .-= beta .* wrkspc.Hold
        wrkspc.Hold .= wrkspc.H
        # if simplex_proj
        #     wrkspc.H = condatProj(wrkspc.Hextra + 1 / wrkspc.LH * (wrkspc.WtX - wrkspc.WtW * wrkspc.Hextra), zero(T))
        # else
        #     wrkspc.H = wrkspc.Hextra + 1 / wrkspc.LH * (wrkspc.WtX - wrkspc.WtW * wrkspc.Hextra)
        #     setmin!(wrkspc.H,zero(T))
        # end
        mul!(wrkspc.H,wrkspc.WtW,wrkspc.Hextra)
        wrkspc.H .*= -1
        wrkspc.H .+= wrkspc.WtX
        wrkspc.H ./= wrkspc.LH
        wrkspc.H .+= wrkspc.Hextra
        if simplex_proj
            wrkspc.H .= condatProj(wrkspc.H)
        else
            setmin!(wrkspc.H,zero(T))
        end
        wrkspc.LpH = wrkspc.LH
        iter = iter + 1
    end
    # These operations allocate some memory
    mul!(wrkspc.HHt,wrkspc.H,wrkspc.H')
    mul!(wrkspc.XHt,wrkspc.X,wrkspc.H')
    wrkspc.LW = opnorm(wrkspc.HHt)
    return nothing
end

# BSSMF for Dense Matrices
function bssmf!(wrkspc::Workspace{T}; kws...) where {T<:AbstractFloat}

    normX2 = norm(wrkspc.X)^2
    # Loading parameters
    args = Args_bssmf(; kws...)
    
    @unpack inneriter, maxtime, maxiter, minW, maxW, verbose, addzerocol, centering, simplex_proj = args
    if !isa(minW,Vector) || isnan(minW)
        minW = minimum(wrkspc.X)
    else
        minW = T.(minW)
    end

    if !isa(maxW,Vector) || isnan(maxW)
        maxW = maximum(wrkspc.X)
    else
        maxW = T.(maxW)
    end

    @assert maxiter != Inf || maxtime != Inf

    # Initializing Workspace
    init_workspace!(wrkspc,verbose,minW,maxW)
    # Adding zero column if needed
    if addzerocol
        wrkspc.W = [wrkspc.W zeros(T, m)]
        wrkspc.H = [wrkspc.H; rand(T, n)']
        init_workspace!(wrkspc,verbose,minW,maxW)
    end

    # Error initialization
    err = NaN .* Vector{Float64}(undef, maxiter)
    err_time = copy(err)
    err_time[1] = 0.0
    err[1] = 0.5 * norm(wrkspc.X - wrkspc.W * wrkspc.H)^2
    ety0 = 0.0

    if verbose println("Starting BSSMF (dense)") end
    t0 = time()
    k = 1
    while k < maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end
        
        # Update W
        updateW!(wrkspc,inneriter=inneriter,minW=minW,maxW=maxW,addzerocol=addzerocol)
        # Update H
        updateH!(wrkspc,inneriter=inneriter,simplex_proj=simplex_proj)
        
        k+=1
        # Saving the error
        etz = time() - t0
        err[k] = 0.5 * (normX2 - 2 * sum(wrkspc.W .* wrkspc.XHt) + sum(wrkspc.WtW .* wrkspc.HHt))
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    inan = findfirst(isnan, err_time)
    return err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
end

# BSSMF for Sparse Matrices
function bssmf(X::AbstractSparseMatrix{T},
    r::Integer;
    Wt::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    H::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    kws...) where {T<:AbstractFloat}

    # Loading parameters
    args = Args_bssmf(; kws...)
    @unpack inneriter, maxtime, maxiter, minW, maxW, verbose, addzerocol, train_size,
    train_samples, centering, centeringperrow, simplex_proj = args

    training = ~isnothing(train_size) || ~isempty(train_samples)

    m, n = size(X)
    I, J, V = findnz(X)

    if training
        if ~isempty(train_samples)
            idx_train_set = train_samples
            idx_test_set = setdiff(1:length(V), idx_train_set)
            length_train_set = length(idx_train_set)
            length_test_set = length(idx_test_set)
        else
            length_train_set = floor(Int, length(V) * train_size)
            length_test_set = length(V) - length_train_set
            idx_train_set = shuffle(1:length(V))
            idx_test_set = idx_train_set[1:length_test_set]
            idx_train_set = idx_train_set[length_test_set+1:end]
            @assert length(idx_train_set) == length_train_set
        end
        idx_train_set = idx_train_set[sortperm(idx_train_set)]
        Itest, Jtest, Vtest = I[idx_test_set], J[idx_test_set], V[idx_test_set]
        I, J, V = I[idx_train_set], J[idx_train_set], V[idx_train_set]
        Diff = sparse(I, J, V, m, n)
        I,J,V = findnz(Diff)
    else
        Diff = copy(X) # Will contain (X-WH)[coord]
    end
    X = nothing
    GC.gc()

    if isnan(minW)
        minW = minimum(V)
    else
        minW = T.(minW)
    end
    if centeringperrow && !isa(minW,Vector)
        minW = [minW for i in 1:m]
    end
    if isnan(maxW)
        maxW = maximum(V)
    else
        maxW = T.(maxW)
    end
    if centeringperrow && !isa(maxW,Vector)
        maxW = [maxW for i in 1:m]
    end
    @assert maxiter != Inf || maxtime != Inf

    # Initializing
    Wt = copy(Wt)
    H = copy(H)
    if verbose && (length(Wt) == 0 || length(H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(Wt) == 0
        Wt = T.(powermethod(Diff, randn(T, n, r))')
        minWt = minimum(Wt)
        maxWt = maximum(Wt)
        if maxWt != minWt
            Wt = (!isinf(minW) && !isinf(maxW)) ? minW .+ (maxW - minW) / (maxWt - minWt) * (Wt .- minWt) : Wt
        end
    end
    # Wt = length(Wt) == 0 ? T.(powermethod(Diff, randn(T, n, r))') : Wt

    # Init for H
    H = length(H) == 0 ? T.(svd(Wt * Diff).V') : H
    # Projection on the unit simplex
    # H = addzerocol ? H : condatProj(H)
    # Adding zero column if needed
    if addzerocol
        Wt = [Wt; zeros(T, m)']
        H = [H; rand(T, n)']
    end


    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(Wt * Wt')
    k = 1
    Wtold = copy(Wt)
    Hold = copy(H)
    alpha1 = T(1)
    alpha2 = T(1)

    diff = zeros(T, length(V))
    partXmWH!(diff, V, Wt, H, I, J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err_time = NaN .* Vector{Float64}(undef, maxiter + 1)
    err_time[k] = 0.0
    if training
        diff_test = zeros(T, length_test_set)
        partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
        rmse_train = copy(err_time)
        rmse_test = copy(err_time)
        rmse_train[k] = sqrt(1 / length_train_set * sum(diff .^ 2))
        rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
    else
        err = copy(err_time)
        err[k] = 0.5 * norm(diff)^2
    end
    ety0 = 0.0

    if verbose println("Starting BSSMF (sparse)") end
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end

        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wtextra = Wt + beta * (Wt - Wtold)
            # Update Diff
            partXmWH!(Diff, V, Wtextra, H, I, J)
            Wtold = Wt
            Wt = Wtextra + 1 / LW * (H * Diff')
            setminmax!(Wt, minW, maxW)
            if addzerocol Wt[end,:] .= zero(T) end
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end

        if centering
            μ = sum(Wt) / length(Wt)
            V .-= μ
            if training
                Vtest .-= μ
            end
            Wt .-= μ
            Wtold .-= μ
            minW -= μ
            maxW -= μ
            LH = opnorm(Wt * Wt')
            # LpH = LH
        elseif centeringperrow
            μ = vec(sum(Wt,dims=1)) / r
            V -= μ[I]
            if training
                Vtest -= μ[Itest]
            end
            Wt .-= μ'
            Wtold .-= μ'
            minW .-= μ
            maxW .-= μ
            LH = opnorm(Wt * Wt')
        else
            LH = opnorm(Wt * Wt')
        end
        if addzerocol && centering || centeringperrow
            Wt[end,:] .= zero(T)
            Wtold[end,:] .= zero(T)
        end
        # LH = opnorm(Wt)^2

        # Update H
        iter = 1
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            # Update Diff
            partXmWH!(Diff, V, Wt, Hextra, I, J)
            Hold = H
            if simplex_proj
                H = condatProj(Hextra + 1 / LH * (Wt * Diff), zero(T))
            else
                H = Hextra + 1 / LH * (Wt * Diff)
                setmin!(H,zero(T))
            end
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        LW = opnorm(H * H')
        # LW = opnorm(H)^2

        # Saving the error

        etz = time() - t0
        k += 1
        if training
            partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
            rmse_train[k] = sqrt(1 / length_train_set * sum(Diff.nzval .^ 2))
            rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
        else
            err[k] = 0.5 * norm(diff)^2
        end
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    # println("Number of iteration: $k")
    inan = findfirst(isnan, err_time)
    if training
        return Wt', H, rmse_train[1:(isnothing(inan) ? end : inan - 1)], rmse_test[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    else
        return Wt', H, err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    end

end

# BSSMF for Dense Matrices
function bssmf(X::AbstractMatrix{T},
    r::Integer;
    W::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    H::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    kws...) where {T<:AbstractFloat}

    m, n = size(X)
    normX2 = norm(X)^2

    # Loading parameters
    args = Args_bssmf(; kws...)
    
    @unpack inneriter, maxtime, maxiter, minW, maxW, verbose, addzerocol, centering, centeringperrow, simplex_proj = args
    if !isa(minW,Vector) || isnan(minW)
        minW = minimum(X)
    else
        minW = T.(minW)
    end
    if centeringperrow && !isa(minW,Vector)
        minW = [minW for i in 1:m]
    end
    if !isa(maxW,Vector) || isnan(maxW)
        maxW = maximum(X)
    else
        maxW = T.(maxW)
    end
    if centeringperrow && !isa(maxW,Vector)
        maxW = [maxW for i in 1:m]
    end
    @assert maxiter != Inf || maxtime != Inf

    # Initializing
    W = copy(W)
    H = copy(H)
    if centering || centeringperrow
        X=copy(X)
    end
    if verbose && (length(W) == 0 || length(H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(W) == 0
        W = rand(T, m, r)
        oldminW = minimum(W)
        oldmaxW = maximum(W)
        if oldmaxW != oldminW
            W = (!isinf(minW) && !isinf(maxW)) ? minW .+ (maxW - minW) / (oldmaxW - oldminW) * (W .- oldminW) : W
        end
    end
    # W = length(W) == 0 ? rand(T, m, r) : W

    # Init for H
    H = length(H) == 0 ? rand(T, r, n) : H
    # Adding zero column if needed
    if addzerocol
        W = [W zeros(T, m)]
        H = [H; rand(T, n)']
    end


    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(W' * W)
    k = 1
    Wold = copy(W)
    Hold = copy(H)
    HHt = H * H'
    XHt = X * H'
    alpha1 = T(1)
    alpha2 = T(1)

    # Error initialization
    err = NaN .* Vector{Float64}(undef, maxiter)
    err_time = copy(err)
    err_time[k] = 0.0
    err[k] = 0.5 * norm(X - W * H)^2
    ety0 = 0.0

    if verbose println("Starting BSSMF (dense)") end
    t0 = time()
    while k < maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end

        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wextra = W + beta * (W - Wold)
            Wold = W
            W = Wextra + 1 / LW * (XHt - Wextra * HHt)
            setminmax!(W, minW, maxW)
            if addzerocol W[:, end] .= zero(T) end
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end

        if centering
            # This step could be improved
            μ = sum(W) / length(W)
            X .-= μ
            normX2 = norm(X)^2
            W .-= μ
            Wold .-= μ
            minW -= μ
            maxW -= μ
            # LpH = LH
        elseif centeringperrow
            μ = vec(sum(W,dims=2)) / r
            X .-= μ
            normX2 = norm(X)^2
            W .-= μ
            Wold .-= μ
            minW .-= μ
            maxW .-= μ
        end
        if addzerocol && centering || centeringperrow
            W[:, end] .= zero(T)
            Wold[:, end] .= zero(T)
        end

        # Update H
        iter = 1
        WtW = W' * W
        WtX = W' * X
        LH = opnorm(WtW)
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            Hold = H
            if simplex_proj
                H = condatProj(Hextra + 1 / LH * (WtX - WtW * Hextra), zero(T))
            else
                H = Hextra + 1 / LH * (WtX - WtW * Hextra)
                setmin!(H,zero(T))
            end
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end

        HHt = H * H'
        XHt = X * H'
        LW = opnorm(HHt)

        # Saving the error
        etz = time() - t0
        k += 1
        err[k] = 0.5 * (normX2 - 2 * sum(W .* XHt) + sum(WtW .* HHt))
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    inan = findfirst(isnan, err_time)
    return W, H, err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
end

# MCP from TITAN paper
function mcp(X::AbstractSparseMatrix{T},
    r::Integer;
    Wt::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    H::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    kws...) where {T<:AbstractFloat}

    # Loading parameters
    args = Args_bssmf(; kws...)
    @unpack inneriter, maxtime, maxiter, verbose, train_size,
    train_samples = args

    training = ~isnothing(train_size) || ~isempty(train_samples)

    m, n = size(X)
    I, J, V = findnz(X)
    θ = T(5)
    λ = T(0.1)
    if training
        if ~isempty(train_samples)
            idx_train_set = train_samples
            idx_test_set = setdiff(1:length(V), idx_train_set)
            length_train_set = length(idx_train_set)
            length_test_set = length(idx_test_set)
        else
            length_train_set = floor(Int, length(V) * train_size)
            length_test_set = length(V) - length_train_set
            idx_train_set = shuffle(1:length(V))
            idx_test_set = idx_train_set[1:length_test_set]
            idx_train_set = idx_train_set[length_test_set+1:end]
            @assert length(idx_train_set) == length_train_set
        end
        idx_train_set = idx_train_set[sortperm(idx_train_set)]
        Itest, Jtest, Vtest = I[idx_test_set], J[idx_test_set], V[idx_test_set]
        I, J, V = I[idx_train_set], J[idx_train_set], V[idx_train_set]
        Diff = sparse(I, J, V, m, n)
        I,J,V = findnz(Diff)
    else
        Diff = copy(X) # Will contain (X-WH)[coord]
    end
    X = nothing
    GC.gc()

    @assert maxiter != Inf || maxtime != Inf

    # Initializing
    Wt = copy(Wt)
    H = copy(H)
    if verbose && (length(Wt) == 0 || length(H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(Wt) == 0
        Wt = T.(powermethod(Diff, randn(T, n, r))')
    end
    # Wt = length(Wt) == 0 ? T.(powermethod(Diff, randn(T, n, r))') : Wt

    # Init for H
    H = length(H) == 0 ? T.(svd(Wt * Diff).V') : H

    LpW = max(T(1e-4),norm(H * H'))
    LW = LpW
    LpH = max(T(1e-4),norm(Wt * Wt'))
    LH = LpH
    k = 1
    Wtold = copy(Wt)
    Hold = copy(H)
    alpha1 = T(1)

    diff = zeros(T, length(V))
    partXmWH!(diff, V, Wt, H, I, J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err_time = NaN .* Vector{Float64}(undef, maxiter + 1)
    err_time[k] = 0.0
    if training
        diff_test = zeros(T, length_test_set)
        partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
        rmse_train = copy(err_time)
        rmse_test = copy(err_time)
        rmse_train[k] = sqrt(1 / length_train_set * sum(diff .^ 2))
        rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
    else
        err = copy(err_time)
        err[k] = 0.5 * norm(diff)^2
    end
    ety0 = 0.0

    if verbose println("Starting MCP") end
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end

        alpha0 = alpha1
        alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            
            beta = min((alpha1 - 1) / alpha1, sqrt(T(0.9999) * LpW*T(1-1e-15) / LW))
            Wtextra = Wt + beta * (Wt - Wtold)
            # Update Diff
            partXmWH!(Diff, V, Wtextra, H, I, J)
            Wtold = Wt
            Wt = Wtextra + 1 / LW * (H * Diff')
            ww = λ*θ*exp.(-θ*abs.(Wtold))
            Wt = max.(zero(T),abs.(Wt) - ww/LW).*sign.(Wt)
            
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end
        LpH = LH
        LH = max(T(1e-4),norm(Wt * Wt'))

        # Update H
        iter = 1
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            beta = min((alpha1 - 1) / alpha1, sqrt(T(0.9999) * LpH*T(1-1e-15) / LH))
            Hextra = H + beta * (H - Hold)
            # Update Diff
            partXmWH!(Diff, V, Wt, Hextra, I, J)
            Hold = H
            H = Hextra + 1 / LH * (Wt * Diff)
            wh = λ*θ*exp.(-θ*abs.(Hold))
            H = max.(zero(T),abs.(H) - wh/LH).*sign.(H)
            
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        LpW = LW
        LW = max(T(1e-4),norm(H * H'))
        # LW = opnorm(H)^2

        # Saving the error

        etz = time() - t0
        k += 1
        if training
            partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
            rmse_train[k] = sqrt(1 / length_train_set * sum(Diff.nzval .^ 2))
            rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
        else
            err[k] = 0.5 * norm(Diff)^2
        end
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    # println("Number of iteration: $k")
    inan = findfirst(isnan, err_time)
    if training
        return Wt', H, rmse_train[1:(isnothing(inan) ? end : inan - 1)], rmse_test[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    else
        return Wt', H, err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    end

end

# Penalized NMF for Sparse Matrices
function pnmf(X::AbstractSparseMatrix{T},
    r::Integer;
    Wt::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    H::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    kws...) where {T<:AbstractFloat}

    # Loading parameters
    args = Args_bssmf(; kws...)
    @unpack inneriter, maxtime, maxiter, verbose, train_size,
    train_samples, λ = args
    λ = T(λ)
    training = ~isnothing(train_size) || ~isempty(train_samples)

    m, n = size(X)
    I, J, V = findnz(X)

    if training
        if ~isempty(train_samples)
            idx_train_set = train_samples
            idx_test_set = setdiff(1:length(V), idx_train_set)
            length_train_set = length(idx_train_set)
            length_test_set = length(idx_test_set)
        else
            length_train_set = floor(Int, length(V) * train_size)
            length_test_set = length(V) - length_train_set
            idx_train_set = shuffle(1:length(V))
            idx_test_set = idx_train_set[1:length_test_set]
            idx_train_set = idx_train_set[length_test_set+1:end]
            @assert length(idx_train_set) == length_train_set
        end
        idx_train_set = idx_train_set[sortperm(idx_train_set)]
        Itest, Jtest, Vtest = I[idx_test_set], J[idx_test_set], V[idx_test_set]
        I, J, V = I[idx_train_set], J[idx_train_set], V[idx_train_set]
        Diff = sparse(I, J, V, m, n)
        I,J,V = findnz(Diff)
    else
        Diff = copy(X) # Will contain (X-WH)[coord]
    end
    X = nothing
    GC.gc()

    @assert maxiter != Inf || maxtime != Inf

    # Initializing
    Wt = copy(Wt)
    H = copy(H)
    if verbose && (length(Wt) == 0 || length(H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    Wt = length(Wt) == 0 ? T.(powermethod(Diff, randn(T, n, r))') : Wt

    # Init for H
    H = length(H) == 0 ? T.(svd(Wt * Diff).V') : H

    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(Wt * Wt')
    k = 1
    Wtold = copy(Wt)
    Hold = copy(H)
    alpha1 = T(1)
    alpha2 = T(1)

    diff = zeros(T, length(V))
    partXmWH!(diff, V, Wt, H, I, J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err_time = NaN .* Vector{Float64}(undef, maxiter + 1)
    err_time[k] = 0.0
    if training
        diff_test = zeros(T, length_test_set)
        partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
        rmse_train = copy(err_time)
        rmse_test = copy(err_time)
        rmse_train[k] = sqrt(1 / length_train_set * sum(diff .^ 2))
        rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
    else
        err = copy(err_time)
        err[k] = 0.5 * norm(diff)^2
    end
    ety0 = 0.0

    if verbose println("Starting PNMF (sparse)") end
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end

        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wtextra = Wt + beta * (Wt - Wtold)
            # Update Diff
            partXmWH!(Diff, V, Wtextra, H, I, J)
            Wtold = Wt
            Wt = Wtextra + 1 / LW * (H * Diff' - λ.*Wt)
            setmin!(Wt, zero(T))
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end

        # LH = opnorm(Wt)^2
        LH = opnorm(Wt * Wt')

        # Update H
        iter = 1
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            # Update Diff
            partXmWH!(Diff, V, Wt, Hextra, I, J)
            Hold = H
            H = Hextra + 1 / LH * (Wt * Diff - λ.*H)
            setmin!(H, zero(T))
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        LW = opnorm(H * H')
        # LW = opnorm(H)^2

        # Saving the error

        etz = time() - t0
        k += 1
        if training
            partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
            rmse_train[k] = sqrt(1 / length_train_set * sum(Diff.nzval .^ 2))
            rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
        else
            err[k] = 0.5 * norm(diff)^2
        end
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    # println("Number of iteration: $k")
    inan = findfirst(isnan, err_time)
    if training
        return Wt', H, rmse_train[1:(isnothing(inan) ? end : inan - 1)], rmse_test[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    else
        return Wt', H, err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    end

end

# Penalized BSSMF for Sparse Matrices
function pbssmf(X::AbstractSparseMatrix{T},
    r::Integer;
    Wt::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    H::AbstractMatrix{T} = Matrix{T}(undef, 0, 0),
    kws...) where {T<:AbstractFloat}

    # Loading parameters
    args = Args_bssmf(; kws...)
    @unpack inneriter, maxtime, maxiter, minW, maxW, verbose, addzerocol, train_size,
    train_samples, centering, centeringperrow, simplex_proj, λ = args
    λ = T(λ)
    training = ~isnothing(train_size) || ~isempty(train_samples)

    m, n = size(X)
    I, J, V = findnz(X)

    if training
        if ~isempty(train_samples)
            idx_train_set = train_samples
            idx_test_set = setdiff(1:length(V), idx_train_set)
            length_train_set = length(idx_train_set)
            length_test_set = length(idx_test_set)
        else
            length_train_set = floor(Int, length(V) * train_size)
            length_test_set = length(V) - length_train_set
            idx_train_set = shuffle(1:length(V))
            idx_test_set = idx_train_set[1:length_test_set]
            idx_train_set = idx_train_set[length_test_set+1:end]
            @assert length(idx_train_set) == length_train_set
        end
        idx_train_set = idx_train_set[sortperm(idx_train_set)]
        Itest, Jtest, Vtest = I[idx_test_set], J[idx_test_set], V[idx_test_set]
        I, J, V = I[idx_train_set], J[idx_train_set], V[idx_train_set]
        Diff = sparse(I, J, V, m, n)
        I,J,V = findnz(Diff)
    else
        Diff = copy(X) # Will contain (X-WH)[coord]
    end
    X = nothing
    GC.gc()

    if isnan(minW)
        minW = minimum(V)
    else
        minW = T.(minW)
    end
    if centeringperrow && !isa(minW,Vector)
        minW = [minW for i in 1:m]
    end
    if isnan(maxW)
        maxW = maximum(V)
    else
        maxW = T.(maxW)
    end
    if centeringperrow && !isa(maxW,Vector)
        maxW = [maxW for i in 1:m]
    end
    @assert maxiter != Inf || maxtime != Inf

    # Initializing
    Wt = copy(Wt)
    H = copy(H)
    if verbose && (length(Wt) == 0 || length(H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(Wt) == 0
        Wt = T.(powermethod(Diff, randn(T, n, r))')
        minWt = minimum(Wt)
        maxWt = maximum(Wt)
        if maxWt != minWt
            Wt = (!isinf(minW) && !isinf(maxW)) ? minW .+ (maxW - minW) / (maxWt - minWt) * (Wt .- minWt) : Wt
        end
    end
    # Wt = length(Wt) == 0 ? T.(powermethod(Diff, randn(T, n, r))') : Wt

    # Init for H
    H = length(H) == 0 ? T.(svd(Wt * Diff).V') : H
    # Projection on the unit simplex
    # H = addzerocol ? H : condatProj(H)
    # Adding zero column if needed
    if addzerocol
        Wt = [Wt; zeros(T, m)']
        H = [H; rand(T, n)']
    end


    LpW = opnorm(H * H')
    LW = LpW
    LpH = opnorm(Wt * Wt')
    k = 1
    Wtold = copy(Wt)
    Hold = copy(H)
    alpha1 = T(1)
    alpha2 = T(1)

    diff = zeros(T, length(V))
    partXmWH!(diff, V, Wt, H, I, J) # Computing (X-WH)[coord] and storing it in Diff

    # Error initialization
    err_time = NaN .* Vector{Float64}(undef, maxiter + 1)
    err_time[k] = 0.0
    if training
        diff_test = zeros(T, length_test_set)
        partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
        rmse_train = copy(err_time)
        rmse_test = copy(err_time)
        rmse_train[k] = sqrt(1 / length_train_set * sum(diff .^ 2))
        rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
    else
        err = copy(err_time)
        err[k] = 0.5 * norm(diff)^2
    end
    ety0 = 0.0

    if verbose println("Starting BSSMF (sparse)") end
    t0 = time()
    while k <= maxiter && err_time[k] <= maxtime

        if verbose display_progress(k, maxiter, err_time[k], maxtime) end

        # Update W
        iter = 1
        # eps0 = norm(W-Wold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0
            alpha0 = alpha1
            alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha1, T(0.9999) * sqrt(LpW / LW))
            Wtextra = Wt + beta * (Wt - Wtold)
            # Update Diff
            partXmWH!(Diff, V, Wtextra, H, I, J)
            Wtold = Wt
            Wt = Wtextra + 1 / LW * (H * Diff' - λ.*Wt)
            setminmax!(Wt, minW, maxW)
            if addzerocol Wt[end,:] .= zero(T) end
            LpW = LW
            # eps_iter = norm(W-Wold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter += 1
        end

        if centering
            μ = sum(Wt) / length(Wt)
            V .-= μ
            if training
                Vtest .-= μ
            end
            Wt .-= μ
            Wtold .-= μ
            minW -= μ
            maxW -= μ
            LH = opnorm(Wt * Wt')
            # LpH = LH
        elseif centeringperrow
            μ = vec(sum(Wt,dims=1)) / r
            V -= μ[I]
            if training
                Vtest -= μ[Itest]
            end
            Wt .-= μ'
            Wtold .-= μ'
            minW .-= μ
            maxW .-= μ
            LH = opnorm(Wt * Wt')
        else
            LH = opnorm(Wt * Wt')
        end
        if addzerocol && centering || centeringperrow
            Wt[end,:] .= zero(T)
            Wtold[end,:] .= zero(T)
        end
        # LH = opnorm(Wt)^2

        # Update H
        iter = 1
        # eps0 = norm(H-Hold)
        # eps_iter = delta_iter*eps0+1
        while iter <= inneriter # && eps_iter > delta_iter*eps0 
            alpha0 = alpha2
            alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
            beta = min((alpha0 - 1) / alpha2, T(0.9999) * sqrt(LpH / LH))
            Hextra = H + beta * (H - Hold)
            # Update Diff
            partXmWH!(Diff, V, Wt, Hextra, I, J)
            Hold = H
            if simplex_proj
                H = condatProj(Hextra + 1 / LH * (Wt * Diff), zero(T))
            else
                H = Hextra + 1 / LH * (Wt * Diff - λ.*H)
                setmin!(H,zero(T))
            end
            LpH = LH
            # eps_iter = norm(H-Hold)
            # if !(eps_iter > delta_iter*eps0 )
            #     println("leaving with iter = $(iter)")
            # end
            iter = iter + 1
        end
        LW = opnorm(H * H')
        # LW = opnorm(H)^2

        # Saving the error

        etz = time() - t0
        k += 1
        if training
            partXmWH!(diff_test, Vtest, Wt, H, Itest, Jtest)
            rmse_train[k] = sqrt(1 / length_train_set * sum(Diff.nzval .^ 2))
            rmse_test[k] = sqrt(1 / length_test_set * sum(diff_test .^ 2))
        else
            err[k] = 0.5 * norm(diff)^2
        end
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end

    # println("Number of iteration: $k")
    inan = findfirst(isnan, err_time)
    if training
        return Wt', H, rmse_train[1:(isnothing(inan) ? end : inan - 1)], rmse_test[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    else
        return Wt', H, err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
    end

end