module halss

include("../common/lpProjs.jl")
using .l1Projs
using LinearAlgebra
using Infiltrator

export titanhals

function titanhals(X,W,H,options)

    # Initializing parameters
    if haskey(options,"lambda")
        lambda = options["lambda"]
    else
        lambda::Float64 = 0.001
    end
    if haskey(options,"delta")
        delta = options["delta"]
    else
        delta::Float64 = 0.01
    end
    if haskey(options,"inneriter")
        inneriter = options["inneriter"]
    else
        inneriter::UInt16 = 10
    end
    if haskey(options,"delta_iter")
        delta_iter = options["delta_iter"]
    else
        delta_iter::Float64 = 1e-6
    end
    nX2 = options["nX"]^2
    maxtime = options["maxtime"]
    m,r = size(W)
    r,n = size(H)

    # Pre-computing and initializing some variables
    IndWi = Matrix{Int16}(undef,r,r-1)
    for i in 1:r
        IndWi[i,:] = [j for j in 1:r if j != i]
    end
    WtW::Matrix{Float64} = W'*W
    WtX::Matrix{Float64} = W'*X
    HHt::Matrix{Float64} = H*H'
    XHt::Matrix{Float64} = X*H'
    Wextra = Vector{Float64}(undef,m)
    Hextra = Vector{Float64}(undef,n)'
    delta_eyer = delta*I
    P::Matrix{Float64} = inv(HHt+delta_eyer)
    C = Matrix{Float64}(undef,r,r)
    LpW = diag(HHt+lambda*P)
    LpH = diag(WtW)
    k = 1
    Wold::Matrix{Float64} = W
    Hold::Matrix{Float64} = H
    alpha1::Float64 = 1.0
    alpha2::Float64 = 1.0

    t0 = time()
    while time()-t0<maxtime
        k += 1

        # Update W
        iter = 1
        eps0 = 0
        eps = 1
        Woldold = W
        while iter <= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha1
            alpha1 = 0.5*(1+sqrt(1+4*alpha0^2))
            for i = 1:r 
                CLU = cholesky(Hermitian(inv( W'*W + delta_eyer )))
                C = CLU.L
                LWi = HHt[i,i]+lambda*(C[[i],:]*C[[i],:]')[1,1]
                beta = min((alpha0-1)/alpha1 , 0.9999*sqrt(LpW[i]/LWi))
                LpW[i] = LWi
                Wi = W[:,i]
                Wextra = beta*(Wi-Wold[:,i])
                Wold[:,i] = Wi
                W[:,i] = condatProj( Wi + Wextra + (XHt[:,i]-W*HHt[:,i]-lambda*W[:,IndWi[i,:]]*C[IndWi[i,:],:]*C[i,:])/LWi,1e-16)
            end
            if iter == 1
                eps0 = norm(W-Woldold)
            end
            eps = norm(W-Woldold)
            iter = iter + 1
        end
        WtW = W'*W
        LH = diag(WtW)
        WtX = W'*X
        # P = inv( WtW + delta_eyer )

        # Update H
        iter = 1
        eps0 = 0
        eps = 1
        Holdold = H
        while iter <= inneriter && eps >= delta_iter*eps0
            alpha0 = alpha2
            alpha2 = 0.5*(1+sqrt(1+4*alpha0^2))
            for i = 1:r
                beta = min((alpha0-1)/alpha2 , 0.9999*sqrt(LpH[i]/LH[i]))
                Hi = H[i,:]'
                Hextra = beta*(Hi-Hold[i,:]')
                Hold[i,:] = Hi
                H[i,:] = max.(Hi + Hextra + (WtX[i,:]'-WtW[i,:]'*H)/LH[i],1e-16)
            end
            if iter == 1
                eps0 = norm(H-Holdold)
            end
            eps = norm(H-Holdold)
            LpH = LH	
            iter = iter + 1
        end
        HHt = H*H'
        XHt = X*H'
    end
    println("Number of iteration: $k")
    return W,H
end

end
