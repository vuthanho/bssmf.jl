# Online algorithm for BSSMF

# #Dependencies :
using SparseArrays
using LinearAlgebra
using Base: @kwdef

using BSSMF

export obssmf

# BSSMF for Sparse Matrices
# NOT ONLINE FOR NOW


@kwdef mutable struct Args_obssmf
    inneriter::Integer = 10             # Maximum number of inner iterations
    delta_iter::AbstractFloat = 1e-3    # Breaking condition for the inner loop
    maxtime::AbstractFloat = 10         # Maximum alloted time (Doesn't take into account the time taken to compute the error)
    maxiter::Integer = 10              # Maximum number of iteration for the whole algorithm (doesn't take into account inneriter)
    minW::AbstractFloat = 0
    maxW::AbstractFloat = 1
    epochs::Integer = 1
    batchsize::Integer = 500
    verbose::Bool = false
end

# Online BSSMF for Dense Matrices
function obssmf( X::AbstractMatrix{T},
                r::Integer;
                W::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                H::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                kws...) where T <: AbstractFloat

    m, n = size(X)
    normX2 = norm(X)^2

    # Loading parameters
    args = Args_obssmf(;kws...)

    inneriter = args.inneriter
    delta_iter = args.delta_iter
    maxtime = args.maxtime
    maxiter = args.maxiter
    epochs  = args.epochs
    batchsize = args.batchsize
    minW    = T(args.minW)
    maxW    = T(args.maxW)
    verbose = args.verbose

    # Initializing
    print("Initializing W and H ... ")
    W = length(W) == 0 ? rand(T,m , r) : W
    oldminW = minimum(W)
    oldmaxW = maximum(W)
    W = minW .+ (maxW - minW) / (oldmaxW - oldminW) * (W .- oldminW)
    H = length(H) == 0 ? rand(T, r, n) : H
    H = condatProj(H)
    println("✓")
    
    # Error initialization
    epoch = 1
    err = NaN .* Vector{Float64}(undef, epochs+1)
    err[epoch] = 0.5 * norm(X - W * H)^2
    ety0 = 0.0

    println("Starting online BSSMF (dense)")
    while epoch <= epochs
        for jbatch in 1:div(size(X,2),batchsize)
            batch = 1+(jbatch-1)*batchsize:jbatch*batchsize
            W, newh = bssmf(X[:,batch],
                            r;
                            W=W,
                            H=H[:,batch],
                            minW=minW,
                            maxW=maxW,
                            maxiter=maxiter,
                            maxtime=maxtime,
                            inneriter=inneriter,
                            delta_iter=delta_iter,
                            verbose=verbose)
            H[:,batch] = newh
        end
        # Saving the error
        epoch += 1
        err[epoch] = 0.5 * norm(X-W*H)^2
    end

    inan = findfirst(isnan,err)
    return W, H, err[1:(isnothing(inan) ? end : inan-1)]
end