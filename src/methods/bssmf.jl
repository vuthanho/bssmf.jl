# Algorithm for BSSMF

# #Dependencies :
using SparseArrays
using LinearAlgebra
using Base: @kwdef
using Parameters
using Random: shuffle
using Statistics:mean

using BSSMF

export bssmf!

function updateW!(wrkspc::Workspace{T};inneriter=1,addzerocol=false,λ=0,centering=false,extra=true) where T <: AbstractFloat
    for _ in 1:inneriter
        alpha0 = wrkspc.alpha1
        wrkspc.alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha1, T(0.9999) * sqrt(wrkspc.LpW / wrkspc.LW))
        # Updating Wextra
        if extra
            wrkspc.Wextra .= (1+beta).*wrkspc.W .- beta.*wrkspc.Wold
        else
            copy!(wrkspc.Wextra, wrkspc.W)
        end
        # Saving W in Wold
        copy!(wrkspc.Wold, wrkspc.W)
        # Updating W
        mul!(wrkspc.W,wrkspc.Wextra,wrkspc.tempHHt)
        wrkspc.W .+= wrkspc.tempXHt
        BSSMF.setminmax!(wrkspc.W, wrkspc.minW, wrkspc.maxW)
        if addzerocol wrkspc.W[:, end] .= zero(T) end
        wrkspc.LpW = wrkspc.LW
    end
    if centering center!(wrkspc) end
    if centering && addzerocol wrkspc.W[:, end] .= zero(T) end
    mul!(wrkspc.WtW,wrkspc.W',wrkspc.W)
    mul!(wrkspc.WtX,wrkspc.W',wrkspc.X)
    wrkspc.LH = opnorm(wrkspc.WtW)
    # precomputing some operations in tempWtW and tempWtX
    wrkspc.tempWtW .= wrkspc.Ir .- 1/wrkspc.LH .* wrkspc.WtW
    copy!(wrkspc.tempWtX,wrkspc.WtX)
    wrkspc.tempWtX ./= wrkspc.LH
    return nothing
end

function updateW!(wrkspc::Sparse_Workspace{T};vad_wrkspc::Union{Nothing,Sparse_Workspace{T}}=nothing,inneriter=1,addzerocol=false,λ=0,centering=false,extra=true) where T <: AbstractFloat
    iter = 1
    while iter <= inneriter
        alpha0 = wrkspc.alpha1
        wrkspc.alpha1 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha1, T(0.9999) * sqrt(wrkspc.LpW / wrkspc.LW))
        # Updating Wextra
        if extra
            wrkspc.Wextra .= (1+beta).*wrkspc.W .- beta.*wrkspc.Wold
        else
            copy!(wrkspc.Wextra, wrkspc.W)
        end
        # Saving W in Wold
        copy!(wrkspc.Wold, wrkspc.W)
        # Updating Diff
        partXmWH!(wrkspc.Diff, wrkspc.V, wrkspc.Wextra, wrkspc.H, wrkspc.I, wrkspc.J)
        # Updating W
        mul!(wrkspc.W,wrkspc.H,wrkspc.Diff')
        iszero(λ) ? nothing : wrkspc.W .-= T(λ) .* wrkspc.Wold
        wrkspc.W ./= wrkspc.LW
        wrkspc.W .+= wrkspc.Wextra
        BSSMF.setminmax!(wrkspc.W, wrkspc.minW, wrkspc.maxW)
        if addzerocol wrkspc.W[end, :] .= zero(T) end
        wrkspc.LpW = wrkspc.LW
        iter += 1
    end
    if centering center!(wrkspc,vad_wrkspc) end
    if addzerocol && centering wrkspc.W[end, :] .= zero(T) end
    mul!(wrkspc.WtW,wrkspc.W,wrkspc.W')
    wrkspc.LH = opnorm(wrkspc.WtW)
    return nothing
end

function updateH!(wrkspc::Workspace{T};inneriter=1,simplex_proj=true,λ=0,extra=true) where T <: AbstractFloat
    iter = 1
    while iter <= inneriter 
        alpha0 = wrkspc.alpha2
        wrkspc.alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha2, T(0.9999) * sqrt(wrkspc.LpH / wrkspc.LH))
        if extra
            wrkspc.Hextra .= (1+beta).*wrkspc.H .- beta.*wrkspc.Hold
        else
            copy!(wrkspc.Hextra, wrkspc.H)
        end
        copy!(wrkspc.Hold, wrkspc.H)
        mul!(wrkspc.H,wrkspc.tempWtW,wrkspc.Hextra)
        wrkspc.H .+= wrkspc.tempWtX
        if simplex_proj
            condatProj!(wrkspc.H)
        else
            setmin!(wrkspc.H,zero(T))
        end
        wrkspc.LpH = wrkspc.LH
        iter = iter + 1
    end
    mul!(wrkspc.HHt,wrkspc.H,wrkspc.H')
    mul!(wrkspc.XHt,wrkspc.X,wrkspc.H')
    wrkspc.LW = opnorm(wrkspc.HHt)
    # precomputing some operations in tempHHt and tempXHt
    wrkspc.tempHHt .= wrkspc.Ir .- 1/wrkspc.LW .* wrkspc.HHt
    copy!(wrkspc.tempXHt,wrkspc.XHt)
    wrkspc.tempXHt ./= wrkspc.LW
    return nothing
end

function updateH!(wrkspc::Sparse_Workspace{T};inneriter=1,simplex_proj=true,λ=0,extra=true) where T <: AbstractFloat
    iter = 1
    while iter <= inneriter 
        alpha0 = wrkspc.alpha2
        wrkspc.alpha2 = T(0.5) * (1 + sqrt(1 + 4 * alpha0^2))
        beta = min((alpha0 - 1) / wrkspc.alpha2, T(0.9999) * sqrt(wrkspc.LpH / wrkspc.LH))
        # Updating Hextra
        if extra
            wrkspc.Hextra .= (1+beta).*wrkspc.H .- beta.*wrkspc.Hold
        else
            copy!(wrkspc.Hextra, wrkspc.H)
        end
        # Saving H in Hold
        copy!(wrkspc.Hold, wrkspc.H)
        # Updating Diff
        partXmWH!(wrkspc.Diff, wrkspc.V, wrkspc.W, wrkspc.Hextra, wrkspc.I, wrkspc.J)
        # Updating H
        mul!(wrkspc.H,wrkspc.W,wrkspc.Diff)
        iszero(λ) && !simplex_proj ? nothing : wrkspc.H .-= T(λ) .* wrkspc.Hold
        wrkspc.H ./= wrkspc.LH
        wrkspc.H .+= wrkspc.Hextra
        if simplex_proj
            wrkspc.H .= condatProj(wrkspc.H)
        else
            setmin!(wrkspc.H,zero(T))
        end
        wrkspc.LpH = wrkspc.LH
        iter = iter + 1
    end
    # These operations allocate some memory
    mul!(wrkspc.HHt,wrkspc.H,wrkspc.H')
    wrkspc.LW = opnorm(wrkspc.HHt)
    return nothing
end

# BSSMF for Dense Matrices
function bssmf!(wrkspc::Workspace{T}; kws...) where {T<:AbstractFloat}
    # Loading parameters
    args = Args_bssmf(; kws...)    
    @unpack inneriter, maxtime, maxiter, verbose, addzerocol, centering, simplex_proj, λ, minW, maxW, extra = args
    wrkspc.minW = T.(minW)
    wrkspc.maxW = T.(maxW)
    @assert maxiter != Inf || maxtime != Inf
    # Initializing Workspace
    init_workspace!(wrkspc,verbose)
    # Adding zero column if needed
    if addzerocol
        wrkspc.W = [wrkspc.W zeros(T, wrkspc.m)]
        wrkspc.H = [wrkspc.H; rand(T, wrkspc.n)']
        wrkspc.r +=1
        init_workspace!(wrkspc,verbose)
    end
    # Error initialization
    err = NaN .* Vector{Float64}(undef, maxiter)
    err_time = copy(err)
    err_time[1] = 0.0
    err[1] = rmse(wrkspc)
    ety0 = 0.0
    if verbose println("Starting BSSMF (dense)") end
    t0 = time()
    k = 1
    while k < maxiter && err_time[k] <= maxtime
        if verbose display_progress(k, maxiter, err_time[k], maxtime) end      
        # Update W
        updateW!(wrkspc,inneriter=inneriter,addzerocol=addzerocol,λ=λ,centering=centering,extra=extra)
        # Update H
        updateH!(wrkspc,inneriter=inneriter,simplex_proj=simplex_proj,λ=λ,extra=extra)     
        k+=1
        # Saving the error
        etz = time() - t0
        err[k] = rmse(wrkspc)
        err_time[k] = err_time[k-1] + etz - ety0
        ety0 = time() - t0
    end
    inan = findfirst(isnan, err_time)
    return err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
end

# BSSMF for Sparse Matrices
function bssmf!(wrkspc::Sparse_Workspace{T},
vad_wrkspc::Union{Nothing,Sparse_Workspace{T}}=nothing; kws...) where {T<:AbstractFloat}
    # Loading parameters
    args = Args_bssmf(; kws...)
    @unpack inneriter, maxtime, maxiter, verbose, addzerocol, centering, simplex_proj, λ, minW, maxW, extra = args
    wrkspc.minW = T.(minW)
    wrkspc.maxW = T.(maxW)
    @assert maxiter != Inf || maxtime != Inf
    # Initializing Workspace
    init_workspace!(wrkspc,verbose)
    # Adding zero column if needed
    if addzerocol
        wrkspc.W = [wrkspc.W; zeros(T, wrkspc.m)']
        wrkspc.H = [wrkspc.H; rand(T, wrkspc.n)']
        wrkspc.r +=1
        init_workspace!(wrkspc,verbose)
    end
    if !isnothing(vad_wrkspc) # If vad_wrkspc is not nothing, we have a vadlidation dataset
        vad_wrkspc.r = wrkspc.r
        linkW_sp_workspaces!(vad_wrkspc,wrkspc)
        init_workspace!(vad_wrkspc,verbose)
    end
    # Error initialization
    err = NaN .* Vector{Float64}(undef, maxiter)
    err_time = copy(err)
    err_time[1] = 0.0
    err[1] = rmse(wrkspc)
    ety0 = 0.0
    # if verbose println("Starting BSSMF (sparse)") end
    t0 = time()
    k = 1
    while k < maxiter && err_time[k] <= maxtime
        if verbose display_progress(k, maxiter, err_time[k], maxtime) end
        # Update W
        updateW!(wrkspc,centering=centering,vad_wrkspc=vad_wrkspc,inneriter=inneriter,addzerocol=addzerocol,λ=λ,extra=extra)
        # Update H
        updateH!(wrkspc,inneriter=inneriter,simplex_proj=simplex_proj,λ=λ,extra=extra)
        # Time after etz is not counted
        etz = time() - t0
        # Estimating H for the validation test if there's one
        if !isnothing(vad_wrkspc)
            init_vad_workspace!(vad_wrkspc,wrkspc)
            updateH!(vad_wrkspc,inneriter=10,simplex_proj=simplex_proj,λ=λ)
            partXmWH!(vad_wrkspc.diff_test,vad_wrkspc.Vtest,vad_wrkspc.W,
                vad_wrkspc.H,vad_wrkspc.Itest,vad_wrkspc.Jtest)
            res = rmse_test(vad_wrkspc)
            # Saving the best W
            if res<vad_wrkspc.rmsebest
                vad_wrkspc.rmsebest = res
                vad_wrkspc.μbest = wrkspc.μ
                copy!(vad_wrkspc.Wbest,vad_wrkspc.W)
                copy!(vad_wrkspc.Hbest,vad_wrkspc.H)
            end
        end
        k+=1
        # Saving the error for the training data
        err[k] = rmse(wrkspc)
        err_time[k] = err_time[k-1] + etz - ety0
        
        ety0 = time() - t0
    end
    inan = findfirst(isnan, err_time)
    return err[1:(isnothing(inan) ? end : inan - 1)], err_time[1:(isnothing(inan) ? end : inan - 1)]
end