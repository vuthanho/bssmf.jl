using LinearAlgebra
using SparseArrays
using Base:@kwdef
using MAT
import Base.Math.clamp!

export AbstractWorkspace, Workspace, init_workspace!, pred!

abstract type AbstractWorkspace end

@kwdef mutable struct Workspace{T<:AbstractFloat} <: AbstractWorkspace where T
    X::AbstractMatrix{T}
    r::Int
    m::Int
    n::Int
    W::Matrix{T}=Matrix{T}(undef, m, r)
    minW=T(NaN)
    maxW=T(NaN)
    H::Matrix{T}=Matrix{T}(undef, r, n)
    Wold::Matrix{T}=Matrix{T}(undef, m, r)
    Wextra::Matrix{T}=Matrix{T}(undef, m, r)
    Hold::Matrix{T}=Matrix{T}(undef, r, n)
    Hextra::Matrix{T}=Matrix{T}(undef, r, n)
    alpha1::T=T(1)
    alpha2::T=T(1)
    LW::T=T(1)
    LH::T=T(1)
    LpW::T=T(1)
    LpH::T=T(1)
    XHt::Matrix{T}=Matrix{T}(undef, m, r)
    HHt::Matrix{T}=Matrix{T}(undef, r, r)
    WtX::Matrix{T}=Matrix{T}(undef, r, n)
    WtW::Matrix{T}=Matrix{T}(undef, r, r)
    tempXHt::Matrix{T}=Matrix{T}(undef, m, r)
    tempHHt::Matrix{T}=Matrix{T}(undef, r, r)
    tempWtX::Matrix{T}=Matrix{T}(undef, r, n)
    tempWtW::Matrix{T}=Matrix{T}(undef, r, r)
    Ir::Matrix{T}=diagm(ones(T,r))
    normX2::T=T(1)
    sumX::T=T(1)
    μ::T=zero(T)
end

@kwdef mutable struct Sparse_Workspace{T} <: AbstractWorkspace where T<:AbstractFloat
    # Training part
    I::Vector{Int}
    J::Vector{Int}
    V::Vector{T}
    r::Int
    m::Int
    n::Int
    W::Matrix{T}=Matrix{T}(undef, 0, 0)
    minW=T(NaN)
    maxW=T(NaN)
    H::Matrix{T}=Matrix{T}(undef, 0, 0)
    Wold::Matrix{T}=Matrix{T}(undef, 0, 0)
    Wextra::Matrix{T}=Matrix{T}(undef, 0, 0)
    Hold::Matrix{T}=Matrix{T}(undef, 0, 0)
    Hextra::Matrix{T}=Matrix{T}(undef, 0, 0)
    Diff::AbstractSparseMatrix{T,Int}=spzeros(T,0,0)
    alpha1::T=T(1)
    alpha2::T=T(1)
    LW::T=T(1)
    LH::T=T(1)
    LpW::T=T(1)
    LpH::T=T(1)
    HHt::Matrix{T}=Matrix{T}(undef, 0, 0)
    WtW::Matrix{T}=Matrix{T}(undef, 0, 0)
    μ::T=zero(T)
    # Testing part
    Itest::Vector{Int}=Vector{Int}(undef, 0)
    Jtest::Vector{Int}=Vector{Int}(undef, 0)
    Vtest::Vector{T}=Vector{T}(undef, 0)
    diff_test::Vector{T}=Vector{T}(undef, 0)
    rmsebest::Real=Inf
    Wbest::Matrix{T}=Matrix{T}(undef, 0, 0)
    Hbest::Matrix{T}=Matrix{T}(undef, 0, 0)
    μbest::T=zero(T)
end

"""
    Workspace(path,r)

Loads the data of a mat file in path and puts it 
in a `Workspace` structure if the data is dense and
in a `Sparse_Workspace` structure if the data is sparse.
"""
function Workspace(path,r;kws...)
    data = matread(path)
    T = eltype(data["data_tr"])
    m,n=size(data["data_tr"])
    if issparse(data["data_tr"])
        I,J,V=findnz(data["data_tr"])
        if haskey(data,"data_te")
            Itest,Jtest,Vtest=findnz(data["data_te"])
            return Sparse_Workspace{T}(I=I,J=J,V=V,r=r,m=m,n=n,
                        Itest=Itest,Jtest=Jtest,Vtest=Vtest;kws...)
        else
            return Sparse_Workspace{T}(I=I,J=J,V=V,r=r,m=m,n=n;kws...)
        end
    else
        return Workspace{T}(X=data["data_tr"],r=r,m=m,n=n;kws...)
    end
end

function Workspace(X::AbstractMatrix{T},r::Int;kws...) where T <: AbstractFloat
    m,n=size(X)
    if issparse(X)
        I,J,V=findnz(X)
        return Sparse_Workspace{T}(I=I,J=J,V=V,r=r,m=m,n=n;kws...)
    else
        return Workspace{T}(X=X,r=r,m=m,n=n;kws...)
    end
end

function init_workspace!(wrkspc::Workspace{T},verbose=false) where T <: AbstractFloat
    # Initializing
    m, n, r = wrkspc.m, wrkspc.n, wrkspc.r
    (isa(wrkspc.minW,Array) || !isnan(wrkspc.minW)) ? nothing : wrkspc.minW = minimum(wrkspc.X)
    (isa(wrkspc.maxW,Array) || !isnan(wrkspc.maxW)) ? nothing : wrkspc.maxW = maximum(wrkspc.X)
    minW,maxW = maximum(wrkspc.minW),minimum(wrkspc.maxW)
    if verbose && (length(wrkspc.W) == 0 || length(wrkspc.H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if !isdefined(wrkspc.W,1)
        wrkspc.W = rand(T, m, r)
        if !isinf(minW) && !isinf(maxW)
            oldminW = minimum(wrkspc.W)
            oldmaxW = maximum(wrkspc.W)
            if oldmaxW != oldminW
                wrkspc.W = minW .+ (maxW - minW) / (oldmaxW - oldminW) * (wrkspc.W .- oldminW)
            end
        end
    end
    # Init for H
    if !isdefined(wrkspc.H,1)
        wrkspc.H = rand(T, r, n)
        condatProj!(wrkspc.H)
    end
    copy!(wrkspc.Wold,wrkspc.W)
    copy!(wrkspc.Hold,wrkspc.H)
    mul!(wrkspc.HHt, wrkspc.H, wrkspc.H')
    mul!(wrkspc.XHt, wrkspc.X, wrkspc.H')
    mul!(wrkspc.WtW, wrkspc.W', wrkspc.W)
    mul!(wrkspc.WtX, wrkspc.W', wrkspc.X)
    wrkspc.LpW = opnorm(wrkspc.HHt)
    wrkspc.LW = wrkspc.LpW
    wrkspc.LpH = opnorm(wrkspc.WtW)
    wrkspc.LH = wrkspc.LpH
    wrkspc.normX2 = norm(wrkspc.X)^2
    wrkspc.sumX = sum(wrkspc.X)

    # precomputing some operations in temp variables
    wrkspc.tempHHt .= wrkspc.Ir .- 1/wrkspc.LW .* wrkspc.HHt
    wrkspc.tempXHt .= 1/wrkspc.LW .* wrkspc.XHt
    wrkspc.tempWtW .= wrkspc.Ir .- 1/wrkspc.LH .* wrkspc.WtW
    wrkspc.tempWtX .= 1/wrkspc.LH .* wrkspc.WtX
end

function init_workspace!(wrkspc::Sparse_Workspace{T},verbose=false) where T  <: AbstractFloat
    # Initializing
    m, n, r = wrkspc.m, wrkspc.n, wrkspc.r
    wrkspc.Diff = sparse(wrkspc.I,wrkspc.J,wrkspc.V,m,n)
    wrkspc.minW = (isa(wrkspc.minW,Array) || !isnan(wrkspc.minW)) ? T.(wrkspc.minW) : minimum(wrkspc.V)
    wrkspc.maxW = (isa(wrkspc.maxW,Array) || !isnan(wrkspc.maxW)) ? T.(wrkspc.maxW) : maximum(wrkspc.V)
    minW,maxW = maximum(wrkspc.minW),minimum(wrkspc.maxW)
    # if verbose && (length(wrkspc.W) == 0 || length(wrkspc.H) == 0) println("Initializing W and/or H ... ") end
    # Init for W
    if length(wrkspc.W) == 0
        wrkspc.W = rand(T, r, m)
        # wrkspc.W = T.(powermethod(wrkspc.Diff, randn(T, n, r))')
        if !isinf(minW) && !isinf(maxW)
            oldminW = minimum(wrkspc.W)
            oldmaxW = maximum(wrkspc.W)
            if oldmaxW != oldminW
                wrkspc.W = minW .+ (maxW - minW) / (oldmaxW - oldminW) * (wrkspc.W .- oldminW)
            end
        end
    end
    # Init for H
    if length(wrkspc.H) == 0
        wrkspc.H = rand(T, r, n)
        condatProj!(wrkspc.H)
        # wrkspc.H = T.(svd(wrkspc.W * wrkspc.Diff).V')
    end
    wrkspc.Wold = copy(wrkspc.W)
    wrkspc.Hold = copy(wrkspc.H)
    wrkspc.Wextra = Matrix{T}(undef,r,m)
    wrkspc.Hextra = Matrix{T}(undef,r,n)
    wrkspc.HHt = wrkspc.H * wrkspc.H'
    wrkspc.WtW = wrkspc.W* wrkspc.W'
    wrkspc.LpW = opnorm(wrkspc.HHt)
    wrkspc.LW = wrkspc.LpW
    wrkspc.LpH = opnorm(wrkspc.WtW)
    wrkspc.LH = wrkspc.LpH
    # If Itest is not empty then this is some
    # validation data
    if !isempty(wrkspc.Itest)
        wrkspc.diff_test = Vector{T}(undef,length(wrkspc.Itest))
        wrkspc.Wbest = Matrix{T}(undef, r, m)
        wrkspc.Hbest = Matrix{T}(undef, r, n)
        wrkspc.rmsebest = T(Inf)
    end
end

function linkW_sp_workspaces!(dest_wrkspc::Sparse_Workspace{T},src_wrkspc::Sparse_Workspace{T}) where T <: AbstractFloat
    @assert dest_wrkspc.m==src_wrkspc.m && dest_wrkspc.r==src_wrkspc.r
    dest_wrkspc.W === src_wrkspc.W ? nothing : dest_wrkspc.W = src_wrkspc.W
    return nothing
end

function init_vad_workspace!(vad_wrkspc::Sparse_Workspace{T},tr_wrkspc::Sparse_Workspace{T}) where T <: AbstractFloat
    vad_wrkspc.alpha2 = T(1)
    # vad_wrkspc.H .= rand.(T) # No reallocation is done here
    vad_wrkspc.Hold .= vad_wrkspc.H
    vad_wrkspc.LpH = tr_wrkspc.LH
    vad_wrkspc.LH = vad_wrkspc.LpH
    vad_wrkspc.minW,vad_wrkspc.maxW = tr_wrkspc.minW,tr_wrkspc.maxW
end

@kwdef mutable struct Args_bssmf
    inneriter = 10                 # Maximum number of inner iterations
    maxtime = Inf            # Maximum alloted time (Doesn't take into account the time taken to compute the error)
    maxiter = 500                  # Maximum number of iteration for the whole algorithm (doesn't take into account inneriter)
    verbose = false                   # Option to add verbose
    addzerocol = false                # Option to add a column of zeros in W
    centering = false                 # Option to center W before updating H
    simplex_proj  = true
    λ = 0 # Penalization weight on W
    minW = NaN
    maxW = NaN
    extra::Bool=true # Option to toggle extrapolation with TITAN
end

function rmse(wrkspc::Workspace{T}) where T <: AbstractFloat
    # Possibilité de le calculer en allouant moins de mémoire
    return sqrt(abs(1/(wrkspc.m*wrkspc.n)*(wrkspc.normX2 - 2 * sum(wrkspc.W .* wrkspc.XHt) + sum(wrkspc.WtW .* wrkspc.HHt))))
end

function rmse(wrkspc::Sparse_Workspace{T}) where T <: AbstractFloat
    return sqrt(1/(length(wrkspc.V))*sum(wrkspc.Diff.nzval.^2))
end

function rmse_test(wrkspc::Sparse_Workspace{T}) where T <: AbstractFloat
    return sqrt(1/(length(wrkspc.Vtest))*sum(wrkspc.diff_test.^2))
end

function clamp!(x::AbstractVector, lo::AbstractVector, hi)
    @inbounds for i in eachindex(x)
        x[i] = clamp(x[i], lo[i], hi)
    end
    x
end

function clamp!(x::AbstractVector, lo, hi::AbstractVector)
    @inbounds for i in eachindex(x)
        x[i] = clamp(x[i], lo, hi[i])
    end
    x
end

function clamp!(x::AbstractVector, lo::AbstractVector, hi::AbstractVector)
    @inbounds for i in eachindex(x)
        x[i] = clamp(x[i], lo[i], hi[i])
    end
    x
end


function setminmax!(y::AbstractMatrix{T}, a, b) where T <: AbstractFloat
    if isa(a,Vector) || isa(b,Vector)
        n = size(y,2)
        @views for j in 1:n
            clamp!(y[:,j],a,b)
        end
    else
        clamp!(y,a,b)
    end
end

function setmin!(y::AbstractMatrix{T}, a) where T <: AbstractFloat
    clamp!(y,a,Inf)
end

function partXmWH!( diff::SubArray{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat
    
    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else
        @views for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function partXmWH!( diff::AbstractVector{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat
    
    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else  
        @views for k in 1:length(I)
            diff[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function partXmWH!( Diff::AbstractSparseMatrix{T},
    V::AbstractVector{T},
    Wt::AbstractMatrix{T},
    H::AbstractMatrix{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat

    if Threads.nthreads() > 1
        Threads.@threads for k in eachindex(I)
            Diff.nzval[k] = V[k]
            w = @view Wt[:,I[k]]
            h = @view H[:,J[k]]
            for i in axes(Wt,1)
                Diff.nzval[k] -= w[i] * h[i]
            end
            # Diff[I[k],J[k]] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    else
        @views for k in eachindex(I)
            Diff.nzval[k] = V[k] - Wt[:,I[k]]' * H[:,J[k]]
        end
    end
    return nothing
end

function setSval!( dest::AbstractMatrix{T},
    src::AbstractVector{T},
    I::Vector{Int64},
    J::Vector{Int64}) where T <: AbstractFloat

    if Threads.nthreads() > 1
        @views Threads.@threads for k in 1:length(I)
            dest[I[k],J[k]] = src[k]
        end
    else
        @views for k in 1:length(I)
            dest[I[k],J[k]] = src[k]
        end
    end
end

function display_progress(k,maxiter,t,maxtime)
    print(" $(ceil(Int64,100*max(k/maxiter,t/maxtime)))%\r")
end

function center!(wrkspc::AbstractWorkspace,vad_wrkspc::Union{Nothing,Sparse_Workspace}=nothing)
    meanW = mean(wrkspc.W)
    wrkspc.μ += meanW 
    wrkspc.W .-= meanW
    if isa(wrkspc,Workspace)
        wrkspc.X .-= meanW
        wrkspc.normX2 += wrkspc.m*wrkspc.n*meanW^2 -2*meanW*wrkspc.sumX
        wrkspc.sumX -= wrkspc.m*wrkspc.n*meanW
    else
        wrkspc.V .-= meanW
    end
    isa(wrkspc.minW,Array) ? wrkspc.minW .-= meanW : wrkspc.minW -= meanW
    isa(wrkspc.maxW,Array) ? wrkspc.maxW .-= meanW : wrkspc.maxW -= meanW
    if !isnothing(vad_wrkspc)
        vad_wrkspc.Vtest .-= meanW
        vad_wrkspc.V .-= meanW
        isa(vad_wrkspc.minW,Array) ? vad_wrkspc.minW .-= meanW : vad_wrkspc.minW -= meanW
        isa(vad_wrkspc.maxW,Array) ? vad_wrkspc.maxW .-= meanW : vad_wrkspc.maxW -= meanW
        vad_wrkspc.μ = wrkspc.μ
    end
    return nothing
end

function pred!(wrkspc::Sparse_Workspace{T};
    center=false,simplex_proj=true,λ=0,inneriter=200,μ=0) where T <: AbstractFloat
    wrkspc.alpha2 = T(1)
    wrkspc.r = size(wrkspc.W,1) # Handling the case where addzerocol=true
    wrkspc.H = Matrix{T}(undef, 0, 0)
    if center && iszero(μ) @warn "center=true but μ=0." end
    if center && !iszero(μ)
        μ = T(μ)
        wrkspc.V .-= μ
        wrkspc.Vtest .-= μ
    end
    init_workspace!(wrkspc)
    updateH!(wrkspc,inneriter=inneriter,simplex_proj=simplex_proj)
    partXmWH!(wrkspc.diff_test,wrkspc.Vtest,wrkspc.W,
    wrkspc.H,wrkspc.Itest,wrkspc.Jtest)
    return rmse_test(wrkspc)
end
