using BSSMF
using LinearAlgebra

function l1ballproj!(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    if Threads.nthreads() > 1
        Threads.@threads for i in 1:size(y,2)
        # for i in 1:size(y,2)
            if norm(y[:,i],1)>1
                y[:,i] = condatProj(y[:,i],epsilon)
            else
                y[:,i] = max.(y[:,i],epsilon)
            end
        end
    else
        for i in 1:size(y,2)
            if norm(y[:,i],1)>1
                y[:,i] = condatProj(y[:,i],epsilon)
            else
                y[:,i] = max.(y[:,i],epsilon)
            end
        end
    end

end

function condatProj(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    if Threads.nthreads() > 1
        Threads.@threads for i in 1:size(y,2)
            y[:,i] = condatProj(y[:,i],epsilon)
        end
    else
        for i in 1:size(y,2)
            y[:,i] = condatProj(y[:,i],epsilon)
        end
    end
    return y
end

function condatProj(y::AbstractArray{T,1},epsilon::T=zero(T)) where T <: AbstractFloat
    a = 1
    N = size(y,1)
    temp = Array{Int}(undef,2*N)
    vtilde = view(temp,1:N)
    v = view(temp,N+1:2*N)
    # vtilde = Array{Int}(undef,N)
    # v = Array{Int}(undef,N)
    v[1] = 1
    vlength = 1
    vtildelength = 0
    rho = y[1] - a
    for n in 2:N
        if y[n]>rho
            rho += (y[n]-rho)/(vlength+1)
            if rho>y[n]-a
                vlength+=1
                v[vlength] = n
            else
                vtilde[vtildelength+1:vtildelength+vlength] = v[1:vlength]
                vtildelength += vlength
                v[1] = n
                vlength = 1
                rho = y[n]-a
            end
        end
    end
    if vtildelength != 0
        for i in 1:vtildelength
            yv = y[vtilde[i]]
            if yv>rho
                vlength+=1
                v[vlength] = vtilde[i]
                rho += (yv-rho)/vlength
            end
        end
    end
    vlengthold = vlength - 1
    while vlengthold != vlength
        vlengthold = vlength
        i=1
        while i<=vlength
            yv = y[v[i]]
            if yv <= rho
                v[i] = v[vlength]
                vlength -=1
                rho+=(rho-yv)/vlength
            else
                i+=1
            end
        end
    end
    return max.(y.-rho,epsilon);
end;

function condatProj!(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    if Threads.nthreads() > 1
        Threads.@threads for i in 1:size(y,2)
            condatProj!(view(y,:,i),epsilon)
        end
    else
        for i in 1:size(y,2)
            condatProj!(view(y,:,i),epsilon)
        end
    end
    return nothing
end

function condatProj!(y::AbstractArray{T,1},epsilon::T=zero(T)) where T <: AbstractFloat
    a = 1
    N = size(y,1)
    vtilde = Array{Int}(undef,N)
    v = Array{Int}(undef,N)
    v[1] = 1
    vlength = 1
    vtildelength = 0
    rho = y[1] - a
    for n in 2:N
        if y[n]>rho
            rho += (y[n]-rho)/(vlength+1)
            if rho>y[n]-a
                vlength+=1
                v[vlength] = n
            else
                vtilde[vtildelength+1:vtildelength+vlength] = v[1:vlength]
                vtildelength += vlength
                v[1] = n
                vlength = 1
                rho = y[n]-a
            end
        end
    end
    if vtildelength != 0
        for i in 1:vtildelength
            yv = y[vtilde[i]]
            if yv>rho
                vlength+=1
                v[vlength] = vtilde[i]
                rho += (yv-rho)/vlength
            end
        end
    end
    vlengthold = vlength - 1
    while vlengthold != vlength
        vlengthold = vlength
        i=1
        while i<=vlength
            yv = y[v[i]]
            if yv <= rho
                v[i] = v[vlength]
                vlength -=1
                rho+=(rho-yv)/vlength
            else
                i+=1
            end
        end
    end
    for i in 1:N
        yimrho = y[i]-rho
        if yimrho < epsilon
            y[i] = epsilon
        else
            y[i] = yimrho
        end
    end
    return nothing
end;

function simplexProj(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    if size(y,2)==1
        y = simplexProj(y[:,1],epsilon)
        return reshape(y,:,1);
    else
        n = size(y,1)
        lambda = maximum(map(/,cumsum(sort(y,dims=1,rev=true),dims=1).-1,repeat(1:n,1,size(y,2))),dims=1)
        return max.(y.-lambda,epsilon);
    end
end;

function simplexProj(y::AbstractArray{T,1},epsilon::T=zero(T)) where T <: AbstractFloat
    n = size(y,1)
    lambda = maximum(map(/,cumsum(sort(y,rev=true)).-1,1:n))
    return max.(y.-lambda,epsilon);
end;

function simplexProj!(y::AbstractArray{T,2},epsilon::T=zero(T)) where T <: AbstractFloat
    n = size(y,2)
    Threads.@threads for i in 1:n
        simplexProj!(view(y,:,i))
    end
end;

function simplexProj!(y::AbstractArray{T,1},epsilon::T=zero(T)) where T <: AbstractFloat
    n = size(y,1)
    lambda = maximum(map(/,cumsum(sort(y,rev=true)).-1,1:n))
    y.-=lambda
    for i in 1:n
        y[i] = max(y[i],epsilon)
    end
    return nothing
end;

function l2Proj(y::AbstractArray{T,1}) where T <: AbstractFloat
    x = max.(y,0)
    i = 1;
    while i<=length(x)
        if sign(x[i])==true
            x = x./norm(x)
            return x
        else
            i+=1
        end
    end
    x[findmax(y)[2]] = 1
    return x;
end;

function l2Proj(y::AbstractArray{T,2}) where T <: AbstractFloat
    if size(y,2)==1
        x = max.(y,0)
        i = 1;
        while i<=size(x,1)
            if sign(x[i])==true
                x = x./norm(x)
                return x
            else
                i+=1
            end
        end
        x[findmax(y)[2]] = 1
        return x;
    else
        x = max.(y,0)
        for i in 1:size(y,2)
            n2=norm(x[:,i])
            if n2!=0
                x[:,i]./=n2
            else
                x[findmax(y[:,i])[2],i] = 1
            end
        end
        return x;
    end
end;

# function l2Proj(y::Array{Float64,1})
#     x = max.(y,0)
#     n2=norm(x)
#     if n2!=0
#         x./=n2
#     else
#         x[findmax(y)[2]] = 1
#     end
#     return x;
# end;