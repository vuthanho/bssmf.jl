using BSSMF
using LinearAlgebra
using SparseArrays

function nnls_fpgm(X::AbstractArray{T},
    W::AbstractArray{T};
    init::AbstractArray{T}=Array{T}(undef, 0),
    options::Dict{String,Number}=Dict{String,Number}()) where T<:AbstractFloat

    # Some pre-computing
    WtW = W'W
    WtX = W'X
    L = opnorm(WtW)
    default_options_nnls_fpgm!(options)
    maxiter = options["maxiter"]
    proj = options["proj"]
    alpha1 = T(options["alpha0"])
    delta = T(options["delta"])
    # Initializing H
    if isempty(init)
        if issparse(X)
            H = max.(zero(T),pinv(W)*X)
        else
            H = max.(zero(T),W\X)
        end
        H *= sum(H.*WtX)/sum(WtW.*(H*H'))
    else
        H = max.(zero(T),init)
    end
    Y = H # Second sequence
    i = 1
    eps0 = zero(T)
    eps = one(T)

    while i <= maxiter && eps >= delta*eps0
        Hp = H
        alpha0 = alpha1
        alpha1 = T(0.5) * (sqrt(alpha0^4+T(4)*alpha0^2) - alpha0^2)
        beta = alpha0*(one(T)-alpha0)/(alpha0^2+alpha1)
        H = Y - (WtW*Y-WtX)/L
        if proj == 1
            H = condatProj(H)
        else
            H = max.(zero(T),H)
        end
        Y = H + beta*(H-Hp)
        if i==1
            eps0 = norm(H-Hp)
        end
        eps = norm(H-Hp)
        i+=1
    end
    return H
end

function default_options_nnls_fpgm!(options::Dict{String,Number})
    if !haskey(options, "delta")
        options["delta"] = 1e-6
    end
    if !haskey(options, "maxiter")
        options["maxiter"] = 500
    end
    if !haskey(options, "proj")
        options["proj"] = 0
    end
    if !haskey(options, "alpha0")
        options["alpha0"] = 0.05
    end
end
