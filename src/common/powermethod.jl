using BSSMF
using SparseArrays
using LinearAlgebra

function powermethod(A::AbstractMatrix{T},
    R::AbstractMatrix{T};
    maxiter::Int=10,
    tol::T=T(1e-9)) where T<:AbstractFloat
    
    Y = A*R
    Q = Matrix(qr(Y).Q)
    for _ in 1:maxiter
        Y = A*(A'*Q)
        Qi = Matrix(qr(Y).Q)
        err = norm(Qi[:,1]-Q[:,1],1)
        Q = Qi
        if err < tol
            break
        end
    end
    return Q
end